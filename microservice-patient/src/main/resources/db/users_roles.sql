CREATE TABLE users_roles (
  role_id VARCHAR(36) NOT NULL,
  user_id VARCHAR(36) NOT NULL,
  CONSTRAINT fk_userol_on_role FOREIGN KEY (role_id) REFERENCES role (role_id),
  CONSTRAINT fk_userol_on_user FOREIGN KEY (user_id) REFERENCES user (user_id)
);