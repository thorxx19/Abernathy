CREATE TABLE user (
  user_id VARCHAR(36) NOT NULL,
  name VARCHAR(15) NULL,
  last_name VARCHAR(15) NULL,
  mail VARCHAR(255) NULL,
  password VARCHAR(255) NULL,
  CONSTRAINT pk_user PRIMARY KEY (user_id)
);