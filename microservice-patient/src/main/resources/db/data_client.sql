INSERT INTO client (client_id, first_name, name, birth_date, date_birth_date_format, genre, address, number_phone)
VALUES
('0293e714-bcdd-4689-8b42-c641a3d15598', 'Test', 'TestEarlyOnset', '2002-06-28 02:00:00', '2002-06-28', 'F', '4 Valley Dr', '400-555-6666'),
('37d987bd-5eed-45b9-832d-da3c542a86a2', 'Test', 'TestNone', '1966-12-31 01:00:00', '1966-12-31', 'F', '1 Brookside St', '100-222-3333'),
('407a7351-bb94-44e3-be64-4eff071310d5', 'Test', 'TestBorderline', '1945-06-24 02:00:00', '1945-06-24', 'M', '2 Hight St', '200-333-4444'),
('fa9bc2ee-1d33-4078-9342-f945b00cf790', 'Test', 'TestInDanger', '2004-06-18 02:00:00', '2004-06-18', 'M', '3 Club Road', '300-444-5555');

