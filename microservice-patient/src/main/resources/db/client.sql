CREATE TABLE client (
  client_id VARCHAR(36) NOT NULL,
  first_name VARCHAR(36) NULL,
  name VARCHAR(36) NULL,
  birth_date DATETIME NOT NULL,
  date_birth_date_format VARCHAR(255) NULL,
  genre VARCHAR(5) NULL,
  address VARCHAR(150) NULL,
  number_phone VARCHAR(12) NULL,
  CONSTRAINT pk_client PRIMARY KEY (client_id)
);

