CREATE TABLE role (
  role_id VARCHAR(36) NOT NULL,
  name VARCHAR(255) NULL,
  CONSTRAINT pk_role PRIMARY KEY (role_id)
);