INSERT INTO user (user_id, name, last_name, mail, password)
VALUES
('00b37f1d-251a-459a-8f00-8f0ea7583a07', 'DUPONT', 'Pierre', 'admin@medilabo.fr', '$2a$10$v6eg99D7N3VUNFFKpZTcqe7eLDLevOYT.E1NiMdc/Jt3ZLm74Ci6e'),
('2dc18ac8-1461-43ce-a3bf-e37812a731db', 'DURANT', 'John', 'doctor@medilabo.fr', '$2a$10$v6eg99D7N3VUNFFKpZTcqe7eLDLevOYT.E1NiMdc/Jt3ZLm74Ci6e'),
('70ab5402-8a9d-4550-99ba-6547b3c861f9', 'DOE', 'John', 'user@medilabo.fr', '$2a$10$v6eg99D7N3VUNFFKpZTcqe7eLDLevOYT.E1NiMdc/Jt3ZLm74Ci6e');