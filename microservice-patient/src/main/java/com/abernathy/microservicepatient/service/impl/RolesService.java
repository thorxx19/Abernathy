package com.abernathy.microservicepatient.service.impl;

import com.abernathy.microservicepatient.dao.IRoleDao;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.service.IRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author o.froidefond
 */
@Service
public class RolesService implements IRolesService {

    @Autowired
    private IRoleDao roleDao;

    /**
     * Méthode qui retourne une liste de roles.
     *
     * @return une liste de roles.
     */
    @Override
    public List<Role> getAllRoles() {
        return roleDao.findAll();
    }

}
