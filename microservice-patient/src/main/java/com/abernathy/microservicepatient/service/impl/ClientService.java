package com.abernathy.microservicepatient.service.impl;

import com.abernathy.microservicepatient.controller.INotesMicroservice;
import com.abernathy.microservicepatient.dao.IClientDao;
import com.abernathy.microservicepatient.dao.IUserDao;
import com.abernathy.microservicepatient.model.Client;
import com.abernathy.microservicepatient.service.IClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Service
@Slf4j
public class ClientService implements IClientService {

    @Autowired
    private IClientDao clientRepository;
    @Autowired
    private IUserDao userDao;

    @Autowired
    private INotesMicroservice notesMicroservice;


    /**
     * methode pour paginer et filter une liste de client
     *
     * @param page   numéro de la page
     * @param size   taille de la page
     * @param filtre filtre a appliquer
     * @return une liste de client
     */
    @Override
    public Page<Client> findAllPagination(int page, int size, String filtre) {
        Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));
        return clientRepository.findByNameContainsOrFirstNameContainsOrAddressContainsAllIgnoreCase(filtre, filtre, filtre, sortedByName);
    }

    /**
     * Méthode pour récupérer un client en fonction d'un id.
     *
     * @param id d'un client
     * @return un Client
     */
    @Override
    public Optional<Client> findByIdClient(UUID id) {
        return clientRepository.findById(id);
    }

    /**
     * Méthode pour ajouter un nouveau client.
     *
     * @param client un object client reçu du front
     * @return un boolean
     */
    @Override
    public boolean saveClient(Client client) {
        if (!clientRepository.existsByFirstNameAndNameAllIgnoreCase(client.getFirstName(), client.getName())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(client.getBirthDate());
            client.setDateBirthDateFormat(dateString);
            clientRepository.save(client);
            return true;
        }
        return false;
    }

    /**
     * Méthode pour mettre a jour un client existant.
     *
     * @param client un object client reçu du front
     * @return un boolean
     */
    @Override
    public boolean updateClient(Client client) {
        if (clientRepository.existsById(client.getId())) {
            clientRepository.save(client);
            return true;
        }
        return false;
    }

    /**
     * Méthode pour supprimer un client.
     *
     * @param id d'un client
     * @return un boolean
     */
    @Override
    public boolean deleteClient(UUID id) {
        if (clientRepository.existsById(id)) {
            clientRepository.deleteById(id);
            notesMicroservice.deleteNotes(id);
            return true;
        }
        return false;
    }
}
