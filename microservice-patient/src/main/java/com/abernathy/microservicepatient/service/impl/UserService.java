package com.abernathy.microservicepatient.service.impl;

import com.abernathy.microservicepatient.dao.IRoleDao;
import com.abernathy.microservicepatient.dao.IUserDao;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.model.User;
import com.abernathy.microservicepatient.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


/**
 * @author o.froidefond
 */
@Service
public class UserService implements IUserService {

    @Autowired
    private IUserDao userDao;
    @Autowired
    private IRoleDao roleDao;

    /**
     * @param page   le N° de la page
     * @param size   la taille de la page
     * @param filtre un filtre de recherche
     * @return une liste d'user paginer
     */
    @Override
    public Page<User> getAllUser(int page, int size, String filtre) {
        Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));
        return userDao.findByNameContainsIgnoreCaseOrLastNameContainsIgnoreCaseOrMailContainsIgnoreCaseOrRoles_NameContainsIgnoreCase(filtre, filtre, filtre, filtre, sortedByName);
    }

    /**
     * @param user un object user
     * @return un boolean
     */
    @Override
    public boolean updateUser(User user) {
        if (userDao.existsById(user.getId())) {
            List<Role> roles = new ArrayList<>();

            List<UUID> idRole = user.getRoles().stream().map(Role::getId).toList();
            user.getRoles().clear();
            Optional<Role> roleModif = roleDao.findById(idRole.get(0));

            roles.add(roleModif.get());
            user.setRoles(roles);
            userDao.save(user);
            return true;
        }
        return false;
    }

    /**
     * @param id l'id de l'user
     * @return un user
     */
    @Override
    public Optional<User> getById(UUID id) {
        return userDao.findById(id);
    }

    /**
     * @param mail un mail d'un user
     * @return un user
     */
    @Override
    public User getByMail(String mail) {
        User user = new User();
        if (userDao.existsByMail(mail)) {
            user = userDao.findByMail(mail);
        }
        return user;
    }

    /**
     * @param user un object user
     * @return un boolean
     */
    @Override
    @Transactional
    public boolean addProfil(User user) {

        List<Role> roles = new ArrayList<>();

        if (userDao.existsByMail(user.getMail())) {
            return false;
        } else {
            //-- role par defaut
            if (roleDao.existsByNameIgnoreCase("ROLE_USER")) {
                Role role = roleDao.findByNameIgnoreCase("ROLE_USER");
                roles.add(role);
            }
            user.setRoles(roles);
            userDao.save(user);
            return true;
        }
    }

    /**
     * methode pour supprimer un utilisateur
     *
     * @param id d'un utilisateur
     * @return un boolean
     */
    @Override
    public boolean deleteUser(UUID id) {
        if (userDao.existsById(id)) {
            userDao.delete(userDao.getReferenceById(id));
            roleDao.deleteByUsers(userDao.getReferenceById(id));
            return true;
        }
        return false;
    }
}
