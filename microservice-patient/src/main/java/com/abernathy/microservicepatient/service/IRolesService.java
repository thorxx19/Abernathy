package com.abernathy.microservicepatient.service;

import com.abernathy.microservicepatient.model.Role;

import java.util.List;

/**
 * @author o.froidefond
 */
public interface IRolesService {
    /**
     * @return un object role
     */
    List<Role> getAllRoles();
}
