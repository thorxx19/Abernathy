package com.abernathy.microservicepatient.service;

import com.abernathy.microservicepatient.model.Client;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IClientService {
    /**
     * @param uuid id du client
     * @return un object client
     */
    Optional<Client> findByIdClient(UUID uuid);

    /**
     * @param client un object client
     * @return un boolean
     */
    boolean saveClient(Client client);

    /**
     * @param client un object client
     * @return un boolean
     */
    boolean updateClient(Client client);

    /**
     * @param id l'id d'un client
     * @return un boolean
     */
    boolean deleteClient(UUID id);

    /**
     * @param page   N° de la page
     * @param size   taille de la page
     * @param filtre un filtre de recherche
     * @return une liste de client paginer
     */
    Page<Client> findAllPagination(int page, int size, String filtre);
}
