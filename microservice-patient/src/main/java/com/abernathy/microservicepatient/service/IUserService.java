package com.abernathy.microservicepatient.service;

import com.abernathy.microservicepatient.model.User;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IUserService {
    /**
     * @param page   le N° de la page
     * @param size   la taille de la page
     * @param filtre un filtre de recherche
     * @return un object user
     */
    Page<User> getAllUser(int page, int size, String filtre);

    /**
     * @param id l'id de l'user
     * @return un object user
     */
    Optional<User> getById(UUID id);

    /**
     * @param mail un mail d'un user
     * @return un object user
     */
    User getByMail(String mail);

    /**
     * @param user un object user
     * @return un boolean
     */
    boolean addProfil(User user);

    /**
     * @param user un object user
     * @return un boolean
     */
    boolean updateUser(User user);

    boolean deleteUser(UUID id);
}
