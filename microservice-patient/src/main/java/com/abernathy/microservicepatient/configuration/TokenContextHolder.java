package com.abernathy.microservicepatient.configuration;

/**
 * @author o.froidefond
 */
public class TokenContextHolder {

    private static final ThreadLocal<String> TOKEN_HOLDER = new ThreadLocal<>();

    /**
     * méthode récupérer un token stoker
     *
     * @return un token
     */
    public static String getToken() {
        return TOKEN_HOLDER.get();
    }

    /**
     * méthode pour save un token
     *
     * @param token un token a save
     */
    public static void setToken(String token) {
        TOKEN_HOLDER.set(token);
    }

    /**
     * méthode pour clear le token
     */
    public static void clearToken() {
        TOKEN_HOLDER.remove();
    }

}
