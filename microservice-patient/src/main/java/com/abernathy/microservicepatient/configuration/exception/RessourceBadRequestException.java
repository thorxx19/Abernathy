package com.abernathy.microservicepatient.configuration.exception;

import com.abernathy.microservicepatient.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RessourceBadRequestException extends RuntimeException {

    private final Message message;

    public RessourceBadRequestException(String message, Message listError) {
        super(message);
        this.message = listError;
    }

    public Message getMessageError() {
        return message;
    }

}
