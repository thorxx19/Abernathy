package com.abernathy.microservicepatient.configuration.exception;

import com.abernathy.microservicepatient.model.Error;
import com.abernathy.microservicepatient.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RessourceNotFoundException extends RuntimeException {

    private final Message message;

    public RessourceNotFoundException(String message, Message listError) {
        super(message);
        this.message = listError;
    }

    public Message getMessageError() {
        return message;
    }

}
