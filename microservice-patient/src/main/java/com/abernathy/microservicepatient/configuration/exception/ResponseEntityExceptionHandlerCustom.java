package com.abernathy.microservicepatient.configuration.exception;



import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.persistence.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseEntityExceptionHandlerCustom extends ResponseEntityExceptionHandler {

    @ExceptionHandler({RessourceNotFoundException.class})
    @ApiResponses(value = {@ApiResponse(responseCode = "404", description = "Ressource Not Found")})
    protected ResponseEntity<Object> handleNotFound(final RessourceNotFoundException ex, final WebRequest request) {
        log.error("Une exception EntityNotFoundException a été levée lors de la requête", ex);
        return handleExceptionInternal(ex,
                ex.getMessageError() == null ? new CustomMessage(ex.getMessage()) : ex.getMessageError(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({RessourceBadRequestException.class})
    @ApiResponses(value = {@ApiResponse(responseCode = "400", description = "Bad Request")})
    protected ResponseEntity<Object> handleNot(final RessourceBadRequestException ex, final WebRequest request) {
        log.error("Une exception EntityBadRequestException a été levée lors de la requête", ex);
        return handleExceptionInternal(ex,
                ex.getMessageError() == null ? new CustomMessage(ex.getMessage()) : ex.getMessageError(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
