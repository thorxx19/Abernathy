package com.abernathy.microservicepatient.configuration;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author o.froidefond
 */
@Configuration
public class FeignClientConfiguration {

    /**
     * méthode pour intercepter le token dans le header d'une requête
     *
     * @return un object requestInterceptor
     */
    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            String token = TokenContextHolder.getToken();
            if (token != null) {
                requestTemplate.header("Authorization", "Bearer " + token);
            }
        };
    }

}
