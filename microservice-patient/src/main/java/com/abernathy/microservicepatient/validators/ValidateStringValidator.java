package com.abernathy.microservicepatient.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author o.froidefond
 */
public class ValidateStringValidator implements ConstraintValidator<ValidateString, String> {

    private List<String> valueList;

    /**
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(ValidateString constraintAnnotation) {
        valueList = new ArrayList<>();
        for (String val : constraintAnnotation.acceptedValues()) {
            valueList.add(val.toUpperCase());
        }
    }

    /**
     * @param value object to validate
     * @param cxt   context in which the constraint is evaluated
     * @return un boolean
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext cxt) {
        return valueList.contains(value.toUpperCase());
    }


}
