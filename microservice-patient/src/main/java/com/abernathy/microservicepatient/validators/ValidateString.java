package com.abernathy.microservicepatient.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * @author o.froidefond
 */
@Documented
@Constraint(validatedBy = ValidateStringValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateString {

    /**
     * @return un tableau de string
     */
    String[] acceptedValues();

    /**
     * @return un string
     */
    String message() default "Valeur non valide";

    /**
     * @return une class
     */
    Class<?>[] groups() default {};

    /**
     * @return une class
     */
    Class<? extends Payload>[] payload() default {};


}
