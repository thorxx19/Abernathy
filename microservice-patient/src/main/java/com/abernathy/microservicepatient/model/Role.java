package com.abernathy.microservicepatient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.type.SqlTypes;

import java.util.*;

/**
 * @author o.froidefond
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@ToString
public class Role {
    @Id
    @UuidGenerator
    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(name = "role_id", nullable = false, updatable = false, columnDefinition = "VARCHAR(36)")
    @NotEmpty
    private UUID id;
    @NotEmpty
    private String name;
    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    @ToString.Exclude
    private Collection<User> users;

    //-- construct

    public Role() {
        this.users = new ArrayList<>();
    }

    //-- Getter and Setter

    public List<User> getUsers() {
        return Collections.unmodifiableList((List<? extends User>) this.users);
    }

    public void setUsers(List<User> users) {
        if (users != null) {
            this.users.clear();
            this.users.addAll(users);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role otherRole = (Role) o;

        return Objects.equals(this.getId(), otherRole.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
