package com.abernathy.microservicepatient.model;

import com.abernathy.microservicepatient.validators.ValidateString;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.type.SqlTypes;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Entity
@Table(name = "client")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Client {

    @Id
    @UuidGenerator
    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(name = "client_id", nullable = false, updatable = false, columnDefinition = "VARCHAR(36)")
    private UUID id;
    @NotEmpty
    @Column(name = "first_name")
    private String firstName;
    @NotEmpty(message = "Le nom n'est pas valide")
    private String name;
    @PastOrPresent
    @Column(name = "birth_date")
    @NotNull(message = "Veuillez rentrer une date")
    private Date birthDate;
    private String dateBirthDateFormat;
    @ValidateString(acceptedValues = {"M", "F"}, message = "Veuillez sélectionner un genre.")
    private String genre;
    @NotEmpty(message = "Veuillez rentrer une adresse valide.")
    private String address;
    @Column(name = "number_phone")
    @Pattern(regexp = "\\b(?:\\d{10}|\\d{3}-\\d{3}-\\d{4})\\b", message = "Veuillez rentrer un numéro au format valide.")
    @NotEmpty(message = "Veuillez rentrer un numéro de téléphone valide.")
    private String numberPhone;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Client client = (Client) o;
        return getId() != null && Objects.equals(getId(), client.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
