package com.abernathy.microservicepatient.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author o.froidefond
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Error {

    private String field;
    private String message;

}
