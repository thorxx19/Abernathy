package com.abernathy.microservicepatient.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author o.froidefond
 */
@Getter
@Setter
@NoArgsConstructor
public class Message {

    private String message;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Error> errors;
    private Object data;

    //-- constructor

    public Message(String message, List<Error> errors, Object data) {

        this.message = message != null ? message : "";
        if (errors != null) {
            this.errors = new ArrayList<>(errors);
        } else {
            this.errors = new ArrayList<>();
        }
        this.data = data;
    }

    //-- Getter and Setter

    public List<Error> getErrors() {
        return new ArrayList<>(errors);
    }

    public void setErrors(List<Error> errors) {
        if (errors != null) {
            this.errors = new ArrayList<>(errors);
        } else {
            this.errors = new ArrayList<>();
        }
    }

}
