package com.abernathy.microservicepatient.controller.impl;

import com.abernathy.microservicepatient.configuration.exception.RessourceBadRequestException;
import com.abernathy.microservicepatient.configuration.exception.RessourceNotFoundException;
import com.abernathy.microservicepatient.controller.IClientController;
import com.abernathy.microservicepatient.model.Client;
import com.abernathy.microservicepatient.model.Error;
import com.abernathy.microservicepatient.model.Message;
import com.abernathy.microservicepatient.service.IClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author o.froidefond
 */
@Tag(name = "Client", description = "Gestion des clients")
@RestController
@RequestMapping("/api/client")
@Slf4j
public class ClientController implements IClientController {

    @Autowired
    private IClientService clientService;

    /**
     * Méthode pour récupérer, paginer et filtrer une liste de client.
     *
     * @param page   n° de la page.
     * @param size   taille de la page.
     * @param filtre filtre global.
     * @return une liste de client.
     */
    @Operation(summary = "Retourne une liste de client paginer", description = "Retourne une liste de client paginer")
    @Override
    @GetMapping("/pagination/all")
    public Page<Client> findAllClient(@RequestParam int page, int size, String filtre) {
        return clientService.findAllPagination(page, size, filtre);
    }

    /**
     * Méthode pour récupérer un client en fonction d'un id.
     *
     * @param id d'un client.
     * @return un Client.
     */
    @Operation(summary = "Retourne un client", description = "Retourne un client en fonction de son Id")
    @Override
    @GetMapping("/{id}")
    public Client findByIdClient(@PathVariable UUID id) {
        return clientService.findByIdClient(id).orElseThrow(() -> new RessourceNotFoundException("Aucun Client trouver", null));
    }

    /**
     * Méthode pour ajouter un nouveau client.
     *
     * @param client un object client reçu du front.
     * @return un 203 ou 400.
     */
    @Operation(summary = "Sauvegarde", description = "Sauvegarde un nouveau client")
    @Override
    @PostMapping
    public Message saveClient(@RequestBody @Valid Client client, BindingResult result) {

        if (result.hasErrors()) {

            List<Error> fields = new ArrayList<>();
            for (FieldError field : result.getFieldErrors()) {
                Error error = new Error();
                error.setMessage(field.getDefaultMessage());
                error.setField(field.getField());
                fields.add(error);
            }
            Message message = new Message("Échec de la création d'un nouveau client.", fields, null);
            throw new RessourceBadRequestException("Échec de la création d'un nouveau client.", message);
        } else {
            if (clientService.saveClient(client)) {
                throw new ResponseStatusException(HttpStatus.CREATED, "Création d'un nouveau client réussie.");
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Échec de la création d'un nouveau client.");
            }
        }
    }

    /**
     * Méthode pour mettre a jour un client existant.
     *
     * @param client un object client reçu du front.
     * @return un 200 ou 404.
     */
    @Operation(summary = "Mise a jour", description = "Mise à jour d'un client déjà connue")
    @Override
    @PutMapping
    public Message updateClient(@RequestBody @Valid Client client, BindingResult result) {

        if (result.hasErrors()) {

            List<Error> fields = new ArrayList<>();
            for (FieldError field : result.getFieldErrors()) {
                Error error = new Error();
                error.setMessage(field.getDefaultMessage());
                error.setField(field.getField());
                fields.add(error);
            }
            Message message = new Message("Échec de la mise à jour.", fields, null);
            throw new RessourceBadRequestException("Échec de la mise à jour.", message);
        } else {
            if (clientService.updateClient(client)) {
                throw new ResponseStatusException(HttpStatus.OK, "Mise à jour réussie.");
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Échec de la mise à jour.");
            }
        }
    }

    /**
     * Méthode pour supprimer un client.
     *
     * @param id d'un client.
     * @return un 200 ou 404.
     */
    @Operation(summary = "Suppression d'un client", description = "Suppression d'un client déjà connue")
    @Override
    @DeleteMapping("/{id}")
    public Message deleteClient(@PathVariable UUID id) {
        if (clientService.deleteClient(id)) {
            throw new ResponseStatusException(HttpStatus.OK, "Suppression d'un client réussie.");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Échec de la suppression d'un client.");
        }
    }
}
