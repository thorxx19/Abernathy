package com.abernathy.microservicepatient.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@FeignClient(value = "gateway")
public interface INotesMicroservice {
    /**
     * @param id l'id d'un client
     */
    @DeleteMapping("microservice-notes/api/posts/all/{id}")
    boolean deleteNotes(@PathVariable("id") UUID id);

}
