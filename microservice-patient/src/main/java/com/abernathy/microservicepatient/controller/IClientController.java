package com.abernathy.microservicepatient.controller;

import com.abernathy.microservicepatient.model.Client;
import com.abernathy.microservicepatient.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IClientController {

    /**
     * @param page   N° de la page
     * @param size   taille de la page
     * @param filtre un filtre de recherche
     * @return une liste de client paginer
     */
    Page<Client> findAllClient(@RequestParam int page, int size, String filtre);

    /**
     * @param id l'id d'un client
     * @return un client
     */
    Client findByIdClient(UUID id);

    /**
     * @param client un object client
     * @param result les erreur dans les champs
     * @return un status
     */
    Message saveClient(Client client, BindingResult result);

    /**
     * @param client un object client
     * @param result les erreur dans les champs
     * @return status
     */
    Message updateClient(Client client, BindingResult result);

    /**
     * @param id l'id d'un client
     * @return status
     */
    Message deleteClient(UUID id);

}
