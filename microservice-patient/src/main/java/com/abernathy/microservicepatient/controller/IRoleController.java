package com.abernathy.microservicepatient.controller;

import com.abernathy.microservicepatient.model.Role;

import java.util.List;

/**
 * @author o.froidefond
 */
public interface IRoleController {
    /**
     * @return une liste de roles
     */
    List<Role> getAllRoles();
}
