package com.abernathy.microservicepatient.controller.impl;

import com.abernathy.microservicepatient.configuration.exception.RessourceNotFoundException;
import com.abernathy.microservicepatient.controller.IUserController;
import com.abernathy.microservicepatient.model.Error;
import com.abernathy.microservicepatient.model.Message;
import com.abernathy.microservicepatient.model.User;
import com.abernathy.microservicepatient.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Tag(name = "User", description = "Gestion des Users")
@RestController
@RequestMapping("/api/user")
public class UserController implements IUserController {

    @Autowired
    private IUserService userService;

    /**
     * Méthode retourne une liste paginer des utilisateurs.
     *
     * @param page   le N° de la page.
     * @param size   la taille de la page.
     * @param filtre le filtre a appliquer.
     * @return une liste d'user paginer
     */
    @Override
    @Operation(summary = "Méthode retourne une liste paginer des utilisateurs", description = "Méthode retourne une liste paginer des utilisateurs")
    @GetMapping("/pagination/all")
    public Page<User> allUser(@RequestParam int page, int size, String filtre) {
        return userService.getAllUser(page, size, filtre);
    }

    /**
     * Méthode pour mettre a jour un utilisateur.
     *
     * @param user   un utilisateur.
     * @param result les erreur dans l'object user.
     * @return un responseEntity.
     */
    @Override
    @PutMapping
    @Transient
    @Operation(summary = "Méthode pour mettre a jour un utilisateur", description = "Méthode pour mettre a jour un utilisateur")
    public ResponseEntity<Message> updateUser(@RequestBody @Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            List<Error> fields = new ArrayList<>();
            for (FieldError field : result.getFieldErrors()) {
                Error error = new Error();
                error.setMessage(field.getDefaultMessage());
                error.setField(field.getField());
                fields.add(error);
            }
            Message message = new Message("Échec de la mise à jour.", fields, null);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } else {
            boolean control = userService.updateUser(user);

            if (control) {
                Message message = new Message("Mise à jour réussie.", null, null);
                return new ResponseEntity<>(message, HttpStatus.OK);
            } else {
                Message message = new Message("Échec de la mise à jour.", null, null);
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        }
    }

    /**
     * Méthode pour récupérer un utilisateur en fonction de sont id.
     *
     * @param id l'id d'un utilisateur.
     * @return l'object user.
     */
    @Override
    @GetMapping("{id}")
    @Operation(summary = "Méthode pour récupérer un utilisateur en fonction de sont id", description = "Méthode pour récupérer un utilisateur en fonction de sont id")
    public User getById(@PathVariable UUID id) {
        return userService.getById(id).orElseThrow(() -> new RessourceNotFoundException("Aucun Utilisateur trouver", null));
    }

    /**
     * Méthode pour récupérer un utilisateur en fonction de sont mail.
     *
     * @param mail le mail d'un utilisateur.
     * @return un object user.
     */
    @Override
    @GetMapping("mail")
    @Operation(summary = "Méthode pour récupérer un utilisateur en fonction de sont mail", description = "Méthode pour récupérer un utilisateur en fonction de sont mail")
    public User getByMail(@RequestParam String mail) {
        return userService.getByMail(mail);
    }

    /**
     * Méthode pour ajouter un utilisateur.
     *
     * @param user un utilisateur.
     * @return un boolean true réussie false échec.
     */
    @Override
    @PostMapping("add")
    @Operation(summary = "Méthode pour ajouter un utilisateur", description = "Méthode pour ajouter un utilisateur")
    public boolean addProfil(@RequestBody @Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            return false;
        } else {
            return userService.addProfil(user);
        }
    }

    /**
     * methode pour supprimer un utilisateur
     *
     * @param id id d'un utilisateur
     */
    @DeleteMapping("{id}")
    @Operation(summary = "Suppression d'un utilisateur", description = "Suppression d'un utilisateur déjà connue")
    @Override
    public void deleteUser(@PathVariable UUID id) {
        if (userService.deleteUser(id)) {
            throw new ResponseStatusException(HttpStatus.OK, "Suppression d'un utilisateur réussie.");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Échec de la suppression d'un utilisateur.");
        }
    }
}
