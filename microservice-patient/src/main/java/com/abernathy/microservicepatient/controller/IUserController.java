package com.abernathy.microservicepatient.controller;


import com.abernathy.microservicepatient.model.Message;
import com.abernathy.microservicepatient.model.User;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IUserController {
    /**
     * @param page   le N° page
     * @param size   la taille de la page
     * @param filtre un filtre de recherche
     * @return une liste de user paginer
     */
    Page<User> allUser(int page, int size, String filtre);

    /**
     * @param user   un object user
     * @param result les erreur dans les champs
     * @return retourne le statut
     */
    ResponseEntity<Message> updateUser(User user, BindingResult result);

    /**
     * @param id l'id de l'user
     * @return un object user
     */
    User getById(UUID id);

    /**
     * @param mail un mail d'un user
     * @return un object user
     */
    User getByMail(String mail);

    /**
     * @param user   un object user
     * @param result les erreur dans les champs
     * @return une responseEntity
     */
    boolean addProfil(User user, BindingResult result);

    /**
     * @param id id d'un utilisateur
     */
    void deleteUser(@PathVariable UUID id);
}
