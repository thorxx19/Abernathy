package com.abernathy.microservicepatient.controller.impl;

import com.abernathy.microservicepatient.controller.IRoleController;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.service.IRolesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author o.froidefond
 */
@Tag(name = "Role", description = "Gestion des Roles")
@RestController
@RequestMapping("/api/roles")
public class RoleController implements IRoleController {

    @Autowired
    private IRolesService rolesService;

    /**
     * Méthode qui retourne une liste de roles.
     *
     * @return une liste de roles.
     */
    @Operation(summary = "Retourne une liste de Roles", description = "Retourne une liste de Roles")
    @Override
    @GetMapping("all")
    public List<Role> getAllRoles() {
        return rolesService.getAllRoles();
    }
}
