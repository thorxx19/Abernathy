package com.abernathy.microservicepatient.dao;

import com.abernathy.microservicepatient.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IUserDao extends JpaRepository<User, UUID> {
    /**
     * @param mail un mail d'un utilisateur
     * @return un boolean
     */
    boolean existsByMail(String mail);

    /**
     * @param mail le mail d'un utilisateur
     * @return un user
     */
    User findByMail(String mail);

    /**
     * @param uuid l'id d'un utilisateur
     * @return un boolean
     */
    @Override
    boolean existsById(UUID uuid);

    /**
     * @param name     un filtre de recherche
     * @param lastName un filtre de recherche
     * @param mail     un filtre de recherche
     * @param name1    un filtre de recherche
     * @param pageable la pagination
     * @return une liste d'user paginer
     */
    Page<User> findByNameContainsIgnoreCaseOrLastNameContainsIgnoreCaseOrMailContainsIgnoreCaseOrRoles_NameContainsIgnoreCase(String name, String lastName, String mail, String name1, Pageable pageable);
}
