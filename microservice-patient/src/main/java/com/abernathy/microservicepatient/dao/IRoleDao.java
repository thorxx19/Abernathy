package com.abernathy.microservicepatient.dao;

import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * @author o.froidefond
 */
public interface IRoleDao extends JpaRepository<Role, UUID> {
    /**
     * @param name le nom d'un role
     * @return un boolean
     */
    boolean existsByNameIgnoreCase(String name);

    /**
     * @param name le nom d'un role
     * @return un role
     */
    Role findByNameIgnoreCase(String name);

    long deleteByUsers(User users);
}
