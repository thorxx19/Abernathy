package com.abernathy.microservicepatient.dao;

import com.abernathy.microservicepatient.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@Repository
public interface IClientDao extends JpaRepository<Client, UUID> {

    /**
     * @param userId   l'id d'un user
     * @param filter   filtre de recherche
     * @param pageable la pagination
     * @return un liste de client paginer
     */
    @Query(value = "SELECT c.* " +
            "FROM (client c inner join user ON c.user_id = user.user_id)" +
            "WHERE c.user_id = :userId " +
            "AND (LOWER(c.name) LIKE LOWER(CONCAT('%', :filter, '%')) OR LOWER(c.first_name) LIKE LOWER(CONCAT('%', :filter, '%')) OR LOWER(c.address) LIKE LOWER(CONCAT('%', :filter, '%'))) " +
            "ORDER BY ?#{#pageable}",
            countQuery = "SELECT COUNT(client.client_id) FROM client WHERE client.user_id = :userId AND (LOWER(client.name) LIKE LOWER(CONCAT('%', :filter, '%')) OR LOWER(client.first_name) LIKE LOWER(CONCAT('%', :filter, '%')) OR LOWER(client.address) LIKE LOWER(CONCAT('%', :filter, '%')))",
            nativeQuery = true)
    Page<Client> findByUserIdWithFilter(@Param("userId") String userId, @Param("filter") String filter, Pageable pageable);

    /**
     * @param name      recherche
     * @param firstName recherche
     * @param address   recherche
     * @param pageable  recherche
     * @return une liste de client filtrer et paginer
     */
    Page<Client> findByNameContainsOrFirstNameContainsOrAddressContainsAllIgnoreCase(String name, String firstName, String address, Pageable pageable);

    /**
     * @param firstName le prénom du client
     * @param name      le nom du client
     * @return un boolean true ou false
     */
    boolean existsByFirstNameAndNameAllIgnoreCase(String firstName, String name);

    /**
     * @param uuid l'id d'un client
     */
    @Override
    void deleteById(UUID uuid);
}
