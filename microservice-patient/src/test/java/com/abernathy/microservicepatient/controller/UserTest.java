package com.abernathy.microservicepatient.controller;

import com.abernathy.microservicepatient.dao.IRoleDao;
import com.abernathy.microservicepatient.dao.IUserDao;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.model.User;
import com.abernathy.microservicepatient.service.IUserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class UserTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private IUserService userService;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IRoleDao roleDao;

    private User user;
    private UUID id;

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setup() {
        User user = new User();
        List<Role> roles = new ArrayList<>();

        user.setName("olivierTest");
        user.setLastName("FROIDEFONDTEST");
        user.setMail("orange@test.fr");
        user.setPassword("test2@O.fr");

        Role role = roleDao.findByNameIgnoreCase("ROLE_USER");
        roles.add(role);
        user.setRoles(roles);

        this.user = userDao.save(user);
        this.id = user.getId();
    }

    @Test
    @DisplayName("Je test la récupération d'un user par sont id")
    void getByIds() {
        try {
            UUID id = user.getId();
            mockMvc.perform(get("/api/user/{id}", id).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath("$.id").value(String.valueOf(id)))
                    .andExpect(jsonPath("$.name").value(user.getName()));
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test la pagination")
    void allUser() {
        try {
            int page = 0;
            int size = 10;
            String filter = "";

            mockMvc.perform(get("/api/user/pagination/all")
                            .param("page", String.valueOf(page))
                            .param("size", String.valueOf(size))
                            .param("filter", filter))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test la récupération d'un user par sont mail")
    void getByMail() {
        try {
            mockMvc.perform(get("/api/user/mail").param("mail", user.getMail()).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath("$.id").value(String.valueOf(id)))
                    .andExpect(jsonPath("$.name").value(user.getName()));
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test un update sur un user")
    void updateUser() {
        try {
            user.setLastName("robert");

            mockMvc.perform(put("/api/user").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.message").value("Mise à jour réussie."));
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test la récupération d'un user avec une erreur")
    void getByIdsErreur() {
        try {
            UUID id = UUID.randomUUID();
            mockMvc.perform(get("/api/user/{id}", id).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test un update sur un user avec une erreur (user non trouver)")
    void updateUserErreur1() {
        try {
            user.setId(UUID.randomUUID());

            mockMvc.perform(put("/api/user").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
                    .andExpect(status().is4xxClientError())
                    .andExpect(jsonPath("$.message").value("Échec de la mise à jour."));
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test un update sur un user avec une erreur")
    void updateUserErreur() {
        try {
            user.setLastName("");

            mockMvc.perform(put("/api/user").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
                    .andExpect(status().is4xxClientError())
                    .andExpect(jsonPath("$.message").value("Échec de la mise à jour."));
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test l'ajout d'un user")
    void addProfil() {
        User userAdd = new User();
        userAdd.setName("olivierTest");
        userAdd.setLastName("FROIDEFONDTEST");
        userAdd.setMail("orange1@test.fr");
        userAdd.setPassword("test2@O.fr");

        try {
            mockMvc.perform(post("/api/user/add").contentType(MediaType.APPLICATION_JSON).content(asJsonString(userAdd)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
        User userDelete = userDao.findByMail(userAdd.getMail());
        userDao.delete(userDelete);
    }

    @Test
    @DisplayName("Je test l'ajout d'un user avec une erreur")
    void addProfilErreur() {

        User userAdd = new User();
        userAdd.setName("olivierTest");
        userAdd.setLastName("FROIDEFONDTEST");
        userAdd.setMail("orange@test.fr");
        userAdd.setPassword("test2@O.fr");
        try {
            mockMvc.perform(post("/api/user/add").contentType(MediaType.APPLICATION_JSON).content(asJsonString(userAdd)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @Test
    @DisplayName("Je test l'ajout d'un user avec une erreur")
    void addProfilErreur1() {

        User userAdd = new User();
        userAdd.setName("");
        userAdd.setLastName("FROIDEFONDTEST");
        userAdd.setMail("orange@test.fr");
        userAdd.setPassword("test2@O.fr");

        try {
            mockMvc.perform(post("/api/user/add").contentType(MediaType.APPLICATION_JSON).content(asJsonString(userAdd)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            log.error("Error : {}", e);
        }
    }

    @AfterEach
    void setupDelete() {
        user.setId(id);
        userDao.delete(user);
    }
}
