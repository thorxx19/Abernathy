package com.abernathy.microservicepatient.controller;


import com.abernathy.microservicepatient.controller.impl.RoleController;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.service.IRolesService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class RoleControllerTest {

    @Mock
    private IRolesService rolesService;

    @InjectMocks
    private RoleController roleController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Nested
    @Tag("GetAllRoles")
    class GetAllRoles {
        @Test
        @DisplayName("Je test la récupération de tout les Roles")
        void testGetAllRoles() {

            List<Role> roles = new ArrayList<>(Collections.emptyList());
            when(rolesService.getAllRoles()).thenReturn(roles);

            // test
            List<Role> result = roleController.getAllRoles();

            // Verification
            assertEquals(roles, result);
            verify(rolesService).getAllRoles();
        }
    }


}
