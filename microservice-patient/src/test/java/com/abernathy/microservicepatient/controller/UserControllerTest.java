package com.abernathy.microservicepatient.controller;

import com.abernathy.microservicepatient.controller.impl.UserController;
import com.abernathy.microservicepatient.model.Message;
import com.abernathy.microservicepatient.model.User;
import com.abernathy.microservicepatient.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class UserControllerTest {

    @Mock
    private IUserService userService;
    @InjectMocks
    private UserController userController;


    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Nested
    @Tag("AllUser")
    class AllUser {
        @Test
        @DisplayName("Je test la récupération de tout les utilisateur")
        void testAllUser() {
            //-- Mocking
            int page = 1;
            int size = 10;
            String filtre = "";
            Page<User> userPage = new PageImpl<>(Collections.emptyList());
            when(userService.getAllUser(page, size, filtre)).thenReturn(userPage);

            //-- test
            Page<User> result = userController.allUser(page, size, filtre);

            //-- verification
            assertEquals(userPage, result);
            verify(userService).getAllUser(page, size, filtre);
        }
    }

    @Nested
    @Tag("UpdateUser")
    class UpdateUser {
        @Test
        @DisplayName("Je test la mise a jour d'un utilisateur")
        void TestUpdateUser() {
            User user = new User();
            Message message = new Message("Mise à jour réussie.", null, null);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            when(userService.updateUser(user)).thenReturn(true);

            ResponseEntity<Message> response = userController.updateUser(user, bindingResult);

            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(message.getMessage(), response.getBody().getMessage());
        }

        @Test
        @DisplayName("Je test la mise a jour d'un utilisateur avec une erreur")
        void TestUpdateUserError() {
            User user = new User();
            Message message = new Message("Échec de la mise à jour.", null, null);
            FieldError fieldError = mock(FieldError.class);
            List<FieldError> fieldErrorList = new ArrayList<>();
            fieldErrorList.add(fieldError);
            BindingResult bindingResult = mock(BindingResult.class);

            when(bindingResult.hasErrors()).thenReturn(true);
            when(bindingResult.getFieldErrors()).thenReturn(fieldErrorList);

            when(userService.updateUser(user)).thenReturn(true);

            ResponseEntity<Message> response = userController.updateUser(user, bindingResult);

            assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
            assertEquals(message.getMessage(), response.getBody().getMessage());
        }

        @Test
        @DisplayName("Je test la mise a jour d'un utilisateur avec une erreur")
        void TestUpdateUserError1() {
            User user = new User();
            Message message = new Message("Échec de la mise à jour.", null, null);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            when(userService.updateUser(user)).thenReturn(false);

            ResponseEntity<Message> response = userController.updateUser(user, bindingResult);

            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            assertEquals(message.getMessage(), response.getBody().getMessage());
        }
    }

    @Nested
    @Tag("FindByIdClient")
    class FindByIdClient {
        @Test
        @DisplayName("Je test la récupération d'un client par sont UUID")
        void testFindByIdClient() {
            // Mocking
            UUID id = UUID.randomUUID();
            User user = new User();
            when(userService.getById(id)).thenReturn(Optional.of(user));

            // Test
            User result = userController.getById(id);

            // Verification
            assertEquals(user, result);
            verify(userService).getById(id);
        }
    }

    @Nested
    @Tag("FindByMailClient")
    class FindByMailClient {
        @Test
        @DisplayName("Je test la récupération d'un client par sont mail")
        void testFindByIdClient() {
            // Mocking
            String mail = "toto@test.fr";
            User user = new User();
            when(userService.getByMail(mail)).thenReturn(user);

            // Test
            User result = userController.getByMail(mail);

            // Verification
            assertEquals(user, result);
            verify(userService).getByMail(mail);
        }
    }

    @Nested
    @Tag("SaveClient")
    class SaveClient {
        @Test
        @DisplayName("Je test la sauvegarde d'un utilisateur")
        void testAddProfil() {
            User user = new User();
            Message message = new Message("Création d'un nouveau utilisateur réussie.", null, null);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            when(userService.addProfil(user)).thenReturn(true);

            boolean response = userController.addProfil(user, bindingResult);

            assertTrue(response);

        }

        @Test
        @DisplayName("Je test la sauvegarde d'un utilisateur")
        void testAddProfilError() {
            User user = new User();
            Message message = new Message("Échec de la création d'un nouveau utilisateur.", null, null);
            FieldError fieldError = mock(FieldError.class);
            List<FieldError> fieldErrorList = new ArrayList<>();
            fieldErrorList.add(fieldError);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(true);
            when(bindingResult.getFieldErrors()).thenReturn(fieldErrorList);

            when(userService.addProfil(user)).thenReturn(false);

            boolean response = userController.addProfil(user, bindingResult);
            assertFalse(response);
        }

        @Test
        @DisplayName("Je test la sauvegarde d'un utilisateur")
        void testAddProfilError1() {
            User user = new User();
            Message message = new Message("Échec de la création d'un nouveau utilisateur.", null, null);

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);


            when(userService.addProfil(user)).thenReturn(false);

            boolean response = userController.addProfil(user, bindingResult);

            assertFalse(response);
        }
    }


}
