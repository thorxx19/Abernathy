package com.abernathy.microservicepatient.controller;

import com.abernathy.microservicepatient.configuration.exception.RessourceBadRequestException;
import com.abernathy.microservicepatient.controller.impl.ClientController;
import com.abernathy.microservicepatient.model.Client;
import com.abernathy.microservicepatient.model.Error;
import com.abernathy.microservicepatient.model.Message;
import com.abernathy.microservicepatient.service.IClientService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class ClientControllerTest {


    @Mock
    private IClientService clientService;

    @InjectMocks
    private ClientController clientController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Nested
    @Tag("FindAllClient")
    class FindAllClient {
        @Test
        @DisplayName("Je test la récupération de tout les clients")
        void testFindAllClient() {
            // Mocking
            int page = 1;
            int size = 10;
            String filtre = "";
            Page<Client> mockedPage = new PageImpl<>(Collections.emptyList());
            when(clientService.findAllPagination(page, size, filtre)).thenReturn(mockedPage);

            // Test
            Page<Client> result = clientController.findAllClient(page, size, filtre);

            // Verification
            assertEquals(mockedPage, result);
            verify(clientService).findAllPagination(page, size, filtre);
        }
    }

    @Nested
    @Tag("FindByIdClient")
    class FindByIdClient {
        @Test
        @DisplayName("Je test la récupération d'un client par sont UUID")
        void testFindByIdClient() {
            // Mocking
            UUID id = UUID.randomUUID();
            Client client = new Client();
            Client mockedClient = client;
            when(clientService.findByIdClient(id)).thenReturn(Optional.of(mockedClient));

            // Test
            Client result = clientController.findByIdClient(id);

            // Verification
            assertEquals(mockedClient, result);
            verify(clientService).findByIdClient(id);
        }
    }

    @Nested
    @Tag("SaveClient")
    class SaveClient {
        @Test
        @DisplayName("Je test la sauvegarde d'un client")
        void testSaveClient() {

            Client client = new Client();

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);
            when(clientService.saveClient(client)).thenReturn(true);

            assertThatThrownBy(() -> clientController.saveClient(client, bindingResult))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.CREATED);
        }

        @Test
        @DisplayName("Je test la sauvegarde d'un client avec une erreur")
        void testSaveClientError() {

            Client client = new Client();
            FieldError fieldError = mock(FieldError.class);
            List<FieldError> fieldErrorList = new ArrayList<>();
            fieldErrorList.add(fieldError);
            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(true);
            when(bindingResult.getFieldErrors()).thenReturn(fieldErrorList);

            when(clientService.saveClient(client)).thenReturn(false);

            assertThatThrownBy(() -> clientController.saveClient(client, bindingResult))
                    .isInstanceOf(RessourceBadRequestException.class)
                    .hasMessage("Échec de la création d'un nouveau client.")
                    .hasFieldOrPropertyWithValue("message", "Échec de la création d'un nouveau client.");
        }

        @Test
        @DisplayName("Je test la sauvegarde d'un client avec une erreur")
        void testSaveClientError1() {

            Client client = new Client();

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            when(clientService.saveClient(client)).thenReturn(false);

            assertThatThrownBy(() -> clientController.saveClient(client, bindingResult))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
        }
    }

    @Nested
    @Tag("UpdateClient")
    class UpdateClient {
        @Test
        @DisplayName("Je test la mise a jour d'un client")
        void testUpdateClient() {

            Client client = new Client();

            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            when(clientService.updateClient(client)).thenReturn(true);

            assertThatThrownBy(() -> clientController.updateClient(client, bindingResult))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.OK);
        }

        @Test
        @DisplayName("Je test la mise a jour d'un client avec un erreur")
        void testUpdateClientError() {

            Client client = new Client();

            // Création d'un objet BindingResult simulé avec des erreurs simulées (dans ce cas, sans erreurs)
            BindingResult bindingResult = mock(BindingResult.class);
            when(bindingResult.hasErrors()).thenReturn(false);

            // Mock du comportement de clientService.saveClient pour retourner true
            when(clientService.updateClient(client)).thenReturn(false);

            assertThatThrownBy(() -> clientController.updateClient(client, bindingResult))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
        }

        @Test
        @DisplayName("Je test la mise a jour d'un client avec une erreur")
        void testUpdateClientError1() {

            Client client = new Client();


            // Création d'un objet BindingResult simulé avec des erreurs simulées (dans ce cas, sans erreurs)
            BindingResult bindingResult = mock(BindingResult.class);
            FieldError fieldError = mock(FieldError.class);
            List<FieldError> fieldErrorList = new ArrayList<>();
            fieldErrorList.add(fieldError);
            when(bindingResult.hasErrors()).thenReturn(true);
            when(bindingResult.getFieldErrors()).thenReturn(fieldErrorList);

            // Mock du comportement de clientService.saveClient pour retourner true
            when(clientService.updateClient(client)).thenReturn(false);

            assertThatThrownBy(() -> clientController.updateClient(client, bindingResult))
                    .isInstanceOf(RessourceBadRequestException.class)
                    .hasMessage("Échec de la mise à jour.")
                    .hasFieldOrPropertyWithValue("message", "Échec de la mise à jour.");
        }
    }

    @Nested
    @Tag("DeleteClient")
    class DeleteClient {
        @Test
        @DisplayName("Je test la suppression d'un client")
        void testDeleteClient() {

            UUID id = UUID.randomUUID();

            when(clientService.deleteClient(id)).thenReturn(true);

            assertThatThrownBy(() -> clientController.deleteClient(id))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.OK);

            Mockito.verify(clientService).deleteClient(id);
        }

        @Test
        @DisplayName("Je test la suppression d'un client avec un erreur")
        void testDeleteClientError() {

            UUID id = UUID.randomUUID();

            when(clientService.deleteClient(id)).thenReturn(false);

            assertThatThrownBy(() -> clientController.deleteClient(id))
                    .isInstanceOf(ResponseStatusException.class)
                    .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);

            Mockito.verify(clientService).deleteClient(id);
        }
    }

}
