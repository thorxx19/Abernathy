package com.abernathy.microservicepatient.service;


import com.abernathy.microservicepatient.controller.INotesMicroservice;
import com.abernathy.microservicepatient.dao.IClientDao;
import com.abernathy.microservicepatient.model.Client;
import com.abernathy.microservicepatient.service.impl.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class ClientServiceTest {

    @Mock
    private IClientDao clientRepository;
    @Mock
    private INotesMicroservice notesMicroservice;

    @InjectMocks
    private ClientService clientService;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Je test la sauvegarde d'un client avec une erreur")
    void testSaveClientError() {
        Client client = new Client();
        client.setFirstName("John");
        client.setName("Doe");
        client.setBirthDate(new Date());

        when(clientRepository.existsByFirstNameAndNameAllIgnoreCase(anyString(), anyString())).thenReturn(true);

        boolean result = clientService.saveClient(client);

        assertFalse(result);
        verify(clientRepository, never()).save(client);
    }

    @Nested
    @Tag("SaveClient")
    class SaveClient {
        @Test
        @DisplayName("Je test la sauvegarde d'un client")
        void testSaveClient() {
            Client client = new Client();
            client.setFirstName("John");
            client.setName("Doe");
            client.setBirthDate(new Date());

            when(clientRepository.existsByFirstNameAndNameAllIgnoreCase(anyString(), anyString())).thenReturn(false);

            boolean result = clientService.saveClient(client);

            assertTrue(result);
            verify(clientRepository).save(client);
        }
    }

    @Nested
    @Tag("DeleteClient")
    class DeleteClient {
        @Test
        @DisplayName("Je test le delete d'un client")
        void testDeleteClient() {
            UUID id = UUID.randomUUID();

            when(clientRepository.existsById(id)).thenReturn(true);
            when(notesMicroservice.deleteNotes(id)).thenReturn(true);

            boolean result = clientService.deleteClient(id);

            assertTrue(result);
            verify(clientRepository).deleteById(id);
            verify(notesMicroservice).deleteNotes(id);
        }

        @Test
        @DisplayName("Je test le delete d'un client")
        void testDeleteClientErreur() {
            UUID id = UUID.randomUUID();
            when(clientRepository.existsById(id)).thenReturn(false);

            boolean result = clientService.deleteClient(id);

            assertFalse(result);
            verify(clientRepository, never()).deleteAllById(Collections.singleton(id));
            verify(notesMicroservice, never()).deleteNotes(id);
        }
    }

    @Nested
    @Tag("UpdateClient")
    class UpdateClient {
        @Test
        @DisplayName("Je test l'update d'un client")
        void testUpdateClient() {
            Client client = new Client();
            client.setId(UUID.randomUUID());
            client.setFirstName("John");
            client.setName("Doe");
            client.setBirthDate(new Date());
            when(clientRepository.existsById(client.getId())).thenReturn(true);

            boolean result = clientService.updateClient(client);

            assertTrue(result);
            verify(clientRepository).existsById(client.getId());
            verify(clientRepository).save(client);
        }

        @Test
        @DisplayName("Je test l'update d'un client avec une erreur")
        void testUpdateClientErreur() {
            Client client = new Client();
            client.setId(UUID.randomUUID());
            client.setFirstName("John");
            client.setName("Doe");
            client.setBirthDate(new Date());
            when(clientRepository.existsById(client.getId())).thenReturn(false);

            boolean result = clientService.updateClient(client);

            assertFalse(result);
            verify(clientRepository).existsById(client.getId());
            verify(clientRepository, never()).save(client);
        }
    }

    @Nested
    @Tag("FindByIdClient")
    class FindByIdClient {
        @Test
        @DisplayName("Je teste la récupération d'un client avec sont id")
        void TestFindByIdClient() {
            Client client = new Client();
            client.setId(UUID.randomUUID());
            client.setFirstName("John");
            client.setName("Doe");
            client.setBirthDate(new Date());
            Optional<Client> mockedClient = Optional.of(client);
            when(clientRepository.findById(client.getId())).thenReturn(mockedClient);

            Optional<Client> result = clientService.findByIdClient(client.getId());

            assertEquals(mockedClient, result);
            verify(clientRepository).findById(client.getId());

        }
    }

    @Nested
    @Tag("FindAllPagination")
    class FindAllPagination {
        @Test
        @DisplayName("Je test la récupération d'une liste de client paginer")
        void testFindAllPagination() {
            int page = 1;
            int size = 10;
            String filtre = "";
            Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));
            Page<Client> mockedPage = new PageImpl<>(Collections.emptyList());
            when(clientRepository.findByNameContainsOrFirstNameContainsOrAddressContainsAllIgnoreCase(filtre, filtre, filtre, sortedByName)).thenReturn(mockedPage);

            Page<Client> result = clientService.findAllPagination(page, size, filtre);

            assertEquals(mockedPage, result);
            verify(clientRepository).findByNameContainsOrFirstNameContainsOrAddressContainsAllIgnoreCase(filtre, filtre, filtre, sortedByName);

        }
    }

}
