package com.abernathy.microservicepatient.service;

import com.abernathy.microservicepatient.dao.IRoleDao;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.service.impl.RolesService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class RolesServiceTest {

    @Mock
    private IRoleDao roleDao;

    @InjectMocks
    private RolesService rolesService;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);
    }

    @Nested
    @Tag("GetAllRoles")
    class GetAllRoles {
        @Test
        @DisplayName("Je test la récupération d'une liste de roles")
        void testGetAllRoles() {
            List<Role> roles = new ArrayList<>(Collections.emptyList());
            when(roleDao.findAll()).thenReturn(roles);

            List<Role> result = rolesService.getAllRoles();

            assertEquals(roles, result);
            verify(roleDao).findAll();

        }
    }

}
