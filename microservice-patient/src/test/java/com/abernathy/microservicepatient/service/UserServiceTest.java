package com.abernathy.microservicepatient.service;

import com.abernathy.microservicepatient.dao.IRoleDao;
import com.abernathy.microservicepatient.dao.IUserDao;
import com.abernathy.microservicepatient.model.Role;
import com.abernathy.microservicepatient.model.User;
import com.abernathy.microservicepatient.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class UserServiceTest {

    @Mock
    private IUserDao userDao;
    @Mock
    private IRoleDao roleDao;
    @InjectMocks
    private UserService userService;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);
    }

    @Nested
    @Tag("GetAllUser")
    class GetAllUser {
        @Test
        @DisplayName("Je test la récupération d'une liste d'user paginer")
        void testGetAllUser() {
            int page = 1;
            int size = 10;
            String filtre = "";
            Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));
            Page<User> mockedPage = new PageImpl<>(Collections.emptyList());
            when(userDao.findByNameContainsIgnoreCaseOrLastNameContainsIgnoreCaseOrMailContainsIgnoreCaseOrRoles_NameContainsIgnoreCase(filtre, filtre, filtre, filtre, sortedByName)).thenReturn(mockedPage);

            Page<User> result = userService.getAllUser(page, size, filtre);

            assertEquals(mockedPage, result);
            verify(userDao).findByNameContainsIgnoreCaseOrLastNameContainsIgnoreCaseOrMailContainsIgnoreCaseOrRoles_NameContainsIgnoreCase(filtre, filtre, filtre, filtre, sortedByName);
        }
    }

    @Nested
    @Tag("UpdateUser")
    class UpdateUser {
        @Test
        @DisplayName("Je test l'update d'un user")
        void testUpdateUser() {

            User user = new User();
            Role role = new Role();
            Set<Role> roles = new HashSet<>();

            role.setName("USER");
            role.setId(UUID.randomUUID());
            roles.add(role);

            user.setId(UUID.randomUUID());
            user.setName("Doe");
            user.setName("John");

            user.setRoles(roles);
            when(userDao.existsById(user.getId())).thenReturn(true);
            when(roleDao.findById(role.getId())).thenReturn(Optional.of(role));

            boolean result = userService.updateUser(user);

            assertTrue(result);
            verify(userDao).existsById(user.getId());
            verify(userDao).save(user);
            verify(roleDao).findById(role.getId());
        }

        @Test
        @DisplayName("Je test l'update d'un user avec une erreur")
        void testUpdateUserErreur() {

            User user = new User();
            Role role = new Role();
            Set<Role> roles = new HashSet<>();

            role.setName("USER");
            role.setId(UUID.randomUUID());
            roles.add(role);

            user.setId(UUID.randomUUID());
            user.setName("Doe");
            user.setName("John");
            user.setRoles(roles);
            when(userDao.existsById(user.getId())).thenReturn(false);


            boolean result = userService.updateUser(user);

            assertFalse(result);
            verify(userDao).existsById(user.getId());
        }
    }

    @Nested
    @Tag("AddProfil")
    class AddProfil {
        @Test
        @DisplayName("Je test l'ajout d'un user")
        void testAddProfil() {
            User user = new User();
            Role role = new Role();
            Set<Role> roles = new HashSet<>();

            role.setName("USER");
            role.setId(UUID.randomUUID());

            user.setId(UUID.randomUUID());
            user.setName("Doe");
            user.setName("John");
            user.setMail("test@test.fr");
            user.setRoles(roles);

            when(userDao.existsByMail(user.getMail())).thenReturn(false);
            when(roleDao.existsByNameIgnoreCase("ROLE_USER")).thenReturn(true);

            boolean result = userService.addProfil(user);

            assertTrue(result);
            verify(userDao).existsByMail(user.getMail());
            verify(roleDao).existsByNameIgnoreCase("ROLE_USER");
            verify(userDao).save(user);
        }

        @Test
        @DisplayName("Je test l'ajout d'un user avec une erreur")
        void testAddProfilErreur() {
            User user = new User();
            Role role = new Role();
            Set<Role> roles = new HashSet<>();

            role.setName("USER");
            role.setId(UUID.randomUUID());

            user.setId(UUID.randomUUID());
            user.setName("Doe");
            user.setName("John");
            user.setMail("test@test.fr");
            user.setRoles(roles);

            when(userDao.existsByMail(user.getMail())).thenReturn(true);

            boolean result = userService.addProfil(user);

            assertFalse(result);
            verify(userDao).existsByMail(user.getMail());

        }
    }

    @Nested
    @Tag("GetByMail")
    class GetByMail {
        @Test
        @DisplayName("Je test la récupération d'un user par sont mail")
        void testGetByMail() {
            User user = new User();
            String mail = "user@medilabo.fr";

            user.setId(UUID.randomUUID());
            user.setName("DOE");
            user.setName("John");
            user.setMail("user@medilabo.fr");

            when(userDao.findByMail(mail)).thenReturn(user);
            when(userDao.existsByMail(mail)).thenReturn(true);

            User result = userService.getByMail(mail);

            assertEquals(user, result);
            verify(userDao).findByMail(mail);
        }
    }

    @Nested
    @Tag("GetById")
    class GetById {
        @Test
        @DisplayName("Je test la récupération d'un user par sont id")
        void testGetById() {
            User user = new User();
            UUID id = UUID.randomUUID();

            user.setId(UUID.randomUUID());
            user.setName("Doe");
            user.setName("John");
            user.setMail("test@test.fr");

            when(userDao.findById(id)).thenReturn(Optional.of(user));

            Optional<User> result = userService.getById(id);

            assertEquals(Optional.of(user), result);
            verify(userDao).findById(id);
        }
    }

}
