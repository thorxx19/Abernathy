#!/bin/bash



# Adresse et port de la dépendance que vous voulez vérifier

DEPENDENCY_HOST="config-server"

DEPENDENCY_PORT="9101"



# Nombre de tentatives de vérification

MAX_RETRIES=5



# Délai entre les tentatives en secondes

RETRY_INTERVAL=10



# Attente pour que la dépendance soit prête

for ((i=0; i<$MAX_RETRIES; i++)); do

  if nc -z $DEPENDENCY_HOST $DEPENDENCY_PORT; then

    echo "La dépendance $DEPENDENCY_HOST:$DEPENDENCY_PORT est prête."

    exec java -jar microservice-patient

  else

    echo "La dépendance n'est pas encore prête, réessai dans $RETRY_INTERVAL secondes..."

    sleep $RETRY_INTERVAL

  fi

done



echo "La dépendance n'a pas pu être vérifiée après $MAX_RETRIES tentatives."

exit 1  # Échec

