#!/bin/bash



# Adresse et port de la dépendance que vous voulez vérifier

DEPENDENCY_HOST="config-server"

DEPENDENCY_PORT="9101"

DEPENDENCY_HOST1="eureka-server"

DEPENDENCY_PORT1="9102"

# Nombre de tentatives de vérification

MAX_RETRIES=7



# Délai entre les tentatives en secondes

RETRY_INTERVAL=10



# Attente pour que la dépendance soit prête

for ((i=0; i<$MAX_RETRIES; i++)); do

  if nc -z $DEPENDENCY_HOST $DEPENDENCY_PORT && nc -z $DEPENDENCY_HOST1 $DEPENDENCY_PORT1; then

    echo "Les dépendances $DEPENDENCY_HOST:$DEPENDENCY_PORT et $DEPENDENCY_HOST1:$DEPENDENCY_PORT1 sont prête."

    exec java -jar gateway

  else

    echo "La dépendance n'est pas encore prête, réessai dans $RETRY_INTERVAL secondes..."

    sleep $RETRY_INTERVAL

  fi

done



echo "La dépendance n'a pas pu être vérifiée après $MAX_RETRIES tentatives."

exit 1  # Échec