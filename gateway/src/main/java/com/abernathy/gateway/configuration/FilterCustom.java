package com.abernathy.gateway.configuration;

import com.abernathy.gateway.model.JwtUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@Component
public class FilterCustom implements WebFilter {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            JwtUserDetails user = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UUID id = user.getId();
        }
        return chain.filter(exchange);
    }
}
