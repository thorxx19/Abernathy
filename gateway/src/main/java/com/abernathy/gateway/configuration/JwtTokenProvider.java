package com.abernathy.gateway.configuration;


import com.abernathy.gateway.model.RoleDto;
import com.abernathy.gateway.model.UserDto;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author o.froidefond
 */
@Component
@Slf4j
public class JwtTokenProvider {

    @Value("${gateway.app.expires}")
    private Long EXPIRES_IN;
    private PrivateKey privateKey;
    private PublicKey publicKey;


    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair kp = keyPairGenerator.genKeyPair();
        this.publicKey = kp.getPublic();
        this.privateKey = kp.getPrivate();
    }

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parserBuilder().setSigningKey(publicKey).build().parseClaimsJws(token).getBody();
    }

    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).getSubject();
    }

    public String generateToken(UserDto user) {

        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", getRoleNames(user.getRoles()));
        return doGenerateToken(claims, user);
    }

    private String doGenerateToken(Map<String, Object> claims, UserDto user) {

        Long expirationTimeLong = EXPIRES_IN;
        final Date createdDate = new Date();
        final Date expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getName() + " " + user.getLastName())
                .setId(String.valueOf(user.getId()))
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(privateKey, SignatureAlgorithm.RS256)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(publicKey).build().parseClaimsJws(token);
            return !isTokenExpired(token);
        } catch (SignatureException | IllegalArgumentException | UnsupportedJwtException | ExpiredJwtException |
                 MalformedJwtException e) {
            return false;
        }
    }

    private boolean isTokenExpired(String token) {
        Date expiration = Jwts.parserBuilder().setSigningKey(publicKey).build().parseClaimsJws(token).getBody().getExpiration();
        return expiration.before(new Date());
    }

    public List<String> getRoleNames(List<RoleDto> roles) {
        return roles.stream()
                .map(RoleDto::getName)
                .collect(Collectors.toList());
    }

}
