package com.abernathy.gateway.configuration;

import com.abernathy.gateway.model.JwtUserDetails;
import com.abernathy.gateway.service.impl.UserDetailsServiceImpl;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author o.froidefond
 */

@Component
@AllArgsConstructor
public class AuthenticationManagerCustom implements ReactiveAuthenticationManager {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    @SuppressWarnings("unchecked")
    public Mono<Authentication> authenticate(Authentication authentication) {
        String authToken = authentication.getCredentials().toString();
        List<GrantedAuthority> authorityList = new ArrayList<>();
        return Mono.just(jwtTokenProvider.validateToken(authToken))
                .filter(valid -> valid)
                .switchIfEmpty(Mono.empty())
                .map(valid -> {
                    String username = jwtTokenProvider.getUsernameFromToken(authToken);
                    Claims claims = jwtTokenProvider.getAllClaimsFromToken(authToken);
                    List<String> roles = claims.get("roles", List.class);
                    for (String role : roles) {
                        authorityList.add(new SimpleGrantedAuthority(role));
                    }
                    Mono<JwtUserDetails> user = userDetailsService.loadUserById(UUID.fromString(claims.getId()));

                    user.subscribe(jwtUserDetails -> {
                        Authentication auth = new UsernamePasswordAuthenticationToken(jwtUserDetails, authentication.getCredentials());
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    });


                    //List<String> rolesMap = claims.get("role", List.class);
                    return new UsernamePasswordAuthenticationToken(
                            username,
                            null,
                            authorityList);
                });
    }
}
