package com.abernathy.gateway.controller.impl;

import com.abernathy.gateway.controller.IAuthController;
import com.abernathy.gateway.model.AuthResponse;
import com.abernathy.gateway.model.UserDto;
import com.abernathy.gateway.model.UserRequest;
import com.abernathy.gateway.service.IAuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author o.froidefond
 */
@RestController
@RequestMapping("api/auth/")
@Tag(name = "Gateway-auth", description = "Gestion de la connection")
public class AuthController implements IAuthController {

    @Autowired
    private IAuthService authService;

    /**
     * Méthode pour obtenir un token de connection
     *
     * @param userRequest les identifiant de connection
     * @return une reponse
     */
    @Override
    @PostMapping("/login")
    @Operation(summary = "Obtention d'un token de connection", description = "Obtention d'un token de connection avec mdp et identifiant")
    public ResponseEntity<AuthResponse> login(@RequestBody @Valid UserRequest userRequest) {
        return authService.login(userRequest);
    }

    /**
     * Méthode pour s'enregister sur l'application
     *
     * @param userDto les info de l'utilisateur
     * @return une reponse
     */
    @PostMapping("/register")
    @Override
    @Operation(summary = "Enregistrement d'un utilisateur", description = "Enregistrement d'un utilisateur")
    public ResponseEntity register(@RequestBody UserDto userDto) {
        if (authService.register(userDto)) {
            return new ResponseEntity<>("Utilisateur enregistré avec succès.", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Mail d'utilisateur déjà utilisé.", HttpStatus.BAD_REQUEST);
        }
    }
}
