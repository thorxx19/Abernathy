package com.abernathy.gateway.controller;

import com.abernathy.gateway.model.PropertiesDto;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author o.froidefond
 */
public interface IGatewayController {

    Mono<List<PropertiesDto>> searchAddress(String q);
}
