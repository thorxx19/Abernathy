package com.abernathy.gateway.controller;


import com.abernathy.gateway.model.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@FeignClient(name = "user", url = "${spring.feign.customer.client}")
public interface IMicroserviceClient {

    @GetMapping("{id}")
    UserDto findById(@PathVariable("id") UUID id);

    @GetMapping("mail")
    UserDto findByMail(@RequestParam("mail") @ModelAttribute String mail);

    @RequestMapping(method = RequestMethod.POST, value = ("add"), consumes = "application/json")
    boolean addProfil(@RequestBody UserDto profil);
}
