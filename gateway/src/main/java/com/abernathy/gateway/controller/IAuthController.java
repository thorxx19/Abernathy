package com.abernathy.gateway.controller;

import com.abernathy.gateway.model.AuthResponse;
import com.abernathy.gateway.model.UserDto;
import com.abernathy.gateway.model.UserRequest;
import org.springframework.http.ResponseEntity;

/**
 * @author o.froidefond
 */
public interface IAuthController {

    ResponseEntity<AuthResponse> login(UserRequest userRequest);

    ResponseEntity register(UserDto userDto);
}
