package com.abernathy.gateway.controller.impl;


import com.abernathy.gateway.controller.IGatewayController;
import com.abernathy.gateway.model.PropertiesDto;
import com.abernathy.gateway.service.IGatewayService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author o.froidefond
 */
@RestController
@RequestMapping("/api/gateway")
@Tag(name = "Gateway-address", description = "Recherche adresse via une api du gouvernement")
public class GatewayController implements IGatewayController {

    @Autowired
    private IGatewayService gatewayService;

    @Override
    @GetMapping("/search-address")
    @Operation(summary = "Retourne une liste d'adresse", description = "Retourne une liste d'adresse en fonction d'un filtre")
    public Mono<List<PropertiesDto>> searchAddress(@RequestParam String q) {
        return gatewayService.searchAddress(q);
    }

}
