package com.abernathy.gateway.service;

import com.abernathy.gateway.model.PropertiesDto;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author o.froidefond
 */
public interface IGatewayService {
    Mono<List<PropertiesDto>> searchAddress(String q);
}
