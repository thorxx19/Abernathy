package com.abernathy.gateway.service.impl;


import com.abernathy.gateway.model.PropertiesDto;
import com.abernathy.gateway.model.SearchAdresse;
import com.abernathy.gateway.service.IGatewayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * @author o.froidefond
 */
@Service
@Slf4j
public class GatewayService implements IGatewayService {


    /**
     * Méthode pour rechercher des adresse sur une api externe du gourvenement
     *
     * @param q le champs recherche
     * @return une liste simplifier de résultat
     */
    public Mono<List<PropertiesDto>> searchAddress(String q) {

        if (q.length() >= 5) {
            try {

                WebClient client = WebClient.builder().baseUrl("https://api-adresse.data.gouv.fr")
                        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                        .build();

                return client.get().uri("/search/?q=" + q + "&limit=5&autocomplete=1")
                        .accept(MediaType.APPLICATION_JSON)
                        .retrieve().bodyToMono(SearchAdresse.class).map(this::modifJson
                        ).onErrorResume(WebClientResponseException.class, e -> {
                            log.error(e.getMessage());
                            return Mono.just(new ArrayList<>());
                        }).onErrorResume(WebClientRequestException.class, e -> {
                            log.error(e.getMessage());
                            return Mono.just(new ArrayList<>());
                        });

            } catch (WebClientResponseException e) {
                log.info(e.getResponseBodyAsString());
            }
        } else {
            return Mono.just(new ArrayList<>());
        }
        return Mono.just(new ArrayList<>());
    }

    //*--------------------------------*
    // Private Method
    //*--------------------------------*

    /**
     * Méthode pour modifier et simplifier le json que je retourne au front
     *
     * @param searchAdresse le résultat de la recherche d'adresse
     * @return une liste d'adresse
     */
    private List<PropertiesDto> modifJson(SearchAdresse searchAdresse) {

        List<PropertiesDto> propertiesDtoList = new ArrayList<>();
        searchAdresse.getFeatures().forEach(feature -> {
            PropertiesDto result = new PropertiesDto();
            result.setLabel(feature.getProperties().getLabel());
            propertiesDtoList.add(result);
        });

        return propertiesDtoList;
    }
}
