package com.abernathy.gateway.service.impl;

import com.abernathy.gateway.configuration.JwtTokenProvider;
import com.abernathy.gateway.controller.IMicroserviceClient;
import com.abernathy.gateway.model.AuthResponse;
import com.abernathy.gateway.model.UserDto;
import com.abernathy.gateway.model.UserRequest;
import com.abernathy.gateway.service.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

/**
 * @author o.froidefond
 */
@Service
public class AuthService implements IAuthService {


    @Autowired
    private IMicroserviceClient gatewayController;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ReactiveAuthenticationManager authenticationManager;

    /**
     * Methode pour gerer le login coter front
     *
     * @param userRequest l'object reçu
     * @return le token
     */
    @Override
    public ResponseEntity<AuthResponse> login(UserRequest userRequest) {

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userRequest.getMail(), userRequest.getPassword());
        Mono<Authentication> auth = authenticationManager.authenticate(authToken);
        AuthResponse authResponse = new AuthResponse();
        auth.subscribe(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication));

        Mono<UserDto> profil = Mono.just(gatewayController.findByMail(userRequest.getMail()));

        if (profil != null) {
            profil.subscribe(userDto -> {
                if (userDto.getId() != null && passwordIsCorrect(userRequest.getPassword(), userDto.getPassword())) {
                    String jwtToken = jwtTokenProvider.generateToken(userDto);
                    authResponse.setMessage("Votre token");
                    authResponse.setAccessToken("Bearer " + jwtToken);
                    authResponse.setUserId(userDto.getId());
                } else {
                    authResponse.setMessage("Mdp ou adresse mail non valide pour cette utilisateur");
                }
            });
        }

        if (authResponse.getMessage().contains("Mdp ou adresse mail non valide pour cette utilisateur")) {
            return new ResponseEntity<>(authResponse, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }


    /**
     * methode pour vérifier si un profil existe déja avec le même mail
     *
     * @param profil le profil a controler
     * @return 201 ou 400
     */
    @Override
    public boolean register(UserDto profil) {
        // encode le password
        encodePassword(profil);
        return gatewayController.addProfil(profil);
    }

    //*--------------------------------*
    // Private Method
    //*--------------------------------*

    /**
     * méthode pour comparer le mdp
     *
     * @param rawPassword     mdp brut
     * @param encodedPassword mdp encoded
     * @return un boolean
     */
    private boolean passwordIsCorrect(String rawPassword, String encodedPassword) {
        // Utilisez un décodeur de hachage bcrypt pour comparer les mots de passe
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        // Comparez le mot de passe brut avec le mot de passe haché
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * methode pour encodee le mdp d'un profil a l'enregistrement
     *
     * @param profil les information d'un utilisateur
     */
    private void encodePassword(UserDto profil) {
        profil.setPassword(passwordEncoder.encode(profil.getPassword()));
    }
}
