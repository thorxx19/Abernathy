package com.abernathy.gateway.service.impl;


import com.abernathy.gateway.controller.IMicroserviceClient;
import com.abernathy.gateway.model.JwtUserDetails;
import com.abernathy.gateway.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@Service
public class UserDetailsServiceImpl implements ReactiveUserDetailsService {

    @Autowired
    private IMicroserviceClient microserviceClient;

    /**
     * Method pour ajouter le mail au token
     *
     * @param mail le mail du profil
     * @return mail du profil
     * @throws UsernameNotFoundException exception
     */
    @Override
    public Mono<UserDetails> findByUsername(String mail) throws UsernameNotFoundException {
        Mono<UserDto> profil = Mono.just(microserviceClient.findByMail(mail));

        return null;
    }

    /**
     * Method pour ajouter l'id au token
     *
     * @param id du profil
     * @return l'id du profil
     */
    public Mono<JwtUserDetails> loadUserById(UUID id) {
        Mono<UserDto> profil = Mono.just(microserviceClient.findById(id));
        return JwtUserDetails.create(profil);
    }

}
