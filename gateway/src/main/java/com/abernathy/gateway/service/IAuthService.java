package com.abernathy.gateway.service;

import com.abernathy.gateway.model.AuthResponse;
import com.abernathy.gateway.model.UserDto;
import com.abernathy.gateway.model.UserRequest;
import org.springframework.http.ResponseEntity;

/**
 * @author o.froidefond
 */
public interface IAuthService {
    ResponseEntity<AuthResponse> login(UserRequest userRequest);

    boolean register(UserDto profil);
}
