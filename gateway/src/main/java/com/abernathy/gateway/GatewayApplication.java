package com.abernathy.gateway;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;

/**
 * @author o.froidefond
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@SecurityScheme(name = "Gateway", scheme = "bearer", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER)
@OpenAPIDefinition(info = @Info(title = "Gateway", version = "1.0.0", description = "Documentation API Gateway v1.0.0"), security = @SecurityRequirement(name = "Gateway"))
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder
                .routes()
                .route(r -> r.path("/patient/v3/api-docs").and().method(HttpMethod.GET).uri("lb://MICROSERVICE-PATIENT"))
                .route(r -> r.path("/notes/v3/api-docs").and().method(HttpMethod.GET).uri("lb://MICROSERVICE-NOTES"))
                .route(r -> r.path("/api/client/**").filters(f -> f.dedupeResponseHeader("Access-Control-Allow-Origin", "RETAIN_UNIQUE")).uri("lb://MICROSERVICE-PATIENT"))
                .route(r -> r.path("/api/user/**").filters(f -> f.dedupeResponseHeader("Access-Control-Allow-Origin", "RETAIN_UNIQUE")).uri("lb://MICROSERVICE-PATIENT"))
                .route(r -> r.path("/api/roles/**").filters(f -> f.dedupeResponseHeader("Access-Control-Allow-Origin", "RETAIN_UNIQUE")).uri("lb://MICROSERVICE-PATIENT"))
                .route(r -> r.path("/api/posts/**").filters(f -> f.dedupeResponseHeader("Access-Control-Allow-Origin", "RETAIN_UNIQUE")).uri("lb://MICROSERVICE-NOTES"))
                .build();
    }

    @Bean
    public Decoder decoder(ObjectMapper objectMapper) {
        return new JacksonDecoder(objectMapper);
    }

    @Bean
    public Encoder encoder(ObjectMapper objectMapper) {
        return new JacksonEncoder(objectMapper);
    }

}
