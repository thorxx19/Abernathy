package com.abernathy.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Geometry {

    private String type;
    private List<Float> coordinates;

}
