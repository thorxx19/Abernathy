package com.abernathy.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Features {

    private String type;
    private Geometry geometry;
    private Properties properties;
}
