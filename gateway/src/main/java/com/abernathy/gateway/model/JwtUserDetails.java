package com.abernathy.gateway.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author o.froidefond
 */

@Setter
@Getter
@NoArgsConstructor
public class JwtUserDetails implements UserDetails {


    private UUID id;
    private String username;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;


    private JwtUserDetails(UUID id, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public static Mono<JwtUserDetails> create(Mono<UserDto> profil) {
        List<GrantedAuthority> authorityList = new ArrayList<>();

        profil.subscribe(userDto -> {

            List<String> roles = userDto.getRoles().stream()
                    .map(RoleDto::getName)
                    .toList();
            for (String role : roles) {
                authorityList.add(new SimpleGrantedAuthority(role));
            }
        });
        return profil.map(doctorDto -> new JwtUserDetails(doctorDto.getId(), doctorDto.getName(), doctorDto.getPassword(), authorityList));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
