package com.abernathy.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Properties {

    private String label;
    private Float score;
    private String housenumber;
    private String id;
    private String name;
    private String postcode;
    private String citycode;
    private Float x;
    private Float y;
    private String city;
    private String context;
    private String type;
    private String importance;
    private String street;

}
