package com.abernathy.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SearchAdresse {

    private String type;
    private String version;
    private List<Features> features;
    private String attribution;
    private String license;
    private String query;
    private int limit;

}
