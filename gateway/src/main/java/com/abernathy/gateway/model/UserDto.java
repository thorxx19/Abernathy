package com.abernathy.gateway.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {


    private UUID id;
    private String name;
    private String lastName;
    private String mail;
    private String password;
    private List<RoleDto> roles;

}
