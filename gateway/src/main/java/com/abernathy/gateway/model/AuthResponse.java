package com.abernathy.gateway.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {

    private String message;
    private UUID userId;
    private String accessToken;
    private Map<String, Boolean> datas;
    private List<Error> errors;
    private Object data;

}
