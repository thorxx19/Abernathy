package com.abernathy.gateway.security;

import com.abernathy.gateway.configuration.AuthenticationManagerCustom;
import com.abernathy.gateway.configuration.FilterCustom;
import com.abernathy.gateway.configuration.JwtAuthenticationEntryPoint;
import com.abernathy.gateway.configuration.SecurityContextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author o.froidefond
 */
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    private static final String[] AUTH_WHITELIST = {
            // -- Swagger UI v2
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/actuator/**",
            "/patient/v3/api-docs",
            "/notes/v3/api-docs"
    };
    @Autowired
    private JwtAuthenticationEntryPoint handler;
    @Autowired
    private ReactiveUserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManagerCustom authenticationManager;
    @Autowired
    private SecurityContextRepository securityContextRepository;
    @Autowired
    private FilterCustom filterCustom;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //*--------------------------------*
    // Private Method
    //*--------------------------------*

    @Bean
    SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {

        http
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .exceptionHandling(exceptionHandlingSpec -> exceptionHandlingSpec.authenticationEntryPoint(handler))
                // ...
                .authorizeExchange((authorize) -> authorize
                        .pathMatchers("api/auth/**").permitAll()
                        .pathMatchers(HttpMethod.DELETE).hasAnyRole("ADMIN", "DOCTOR")
                        .pathMatchers(HttpMethod.PUT).hasAnyRole("ADMIN", "DOCTOR")
                        .pathMatchers(HttpMethod.POST).hasAnyRole("ADMIN", "DOCTOR")
                        .pathMatchers(AUTH_WHITELIST).permitAll()
                        .anyExchange().authenticated()
                ).httpBasic(withDefaults())
                .formLogin(withDefaults());
        http.authenticationManager(authenticationManager);
        http.securityContextRepository(securityContextRepository);
        http.addFilterAfter(filterCustom, SecurityWebFiltersOrder.AUTHENTICATION);
        return http.build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
        config.setAllowedOrigins(List.of("http://localhost:3000/", "http://localhost:9001/"));
        config.setAllowCredentials(true);
        config.addAllowedHeader("*");
        config.addAllowedMethod(HttpMethod.PUT);
        config.addAllowedMethod(HttpMethod.DELETE);
        config.addAllowedMethod(HttpMethod.GET);
        config.addAllowedMethod(HttpMethod.OPTIONS);
        config.addAllowedMethod(HttpMethod.POST);
        config.addAllowedMethod(HttpMethod.PATCH);
        config.addAllowedMethod(HttpMethod.HEAD);
        source.registerCorsConfiguration("/**", config);

        return source;
    }
}