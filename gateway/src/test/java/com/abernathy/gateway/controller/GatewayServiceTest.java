package com.abernathy.gateway.controller;


import com.abernathy.gateway.controller.impl.GatewayController;
import com.abernathy.gateway.model.SearchAdresse;
import com.abernathy.gateway.service.impl.GatewayService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;


@Slf4j
@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = GatewayController.class)
@Import(GatewayService.class)
@ActiveProfiles("test")
public class GatewayServiceTest {

    @Autowired
    WebTestClient webTestClient;
    @MockBean
    private RouteLocator routeLocator;
    @Autowired
    private GatewayService gatewayService;

    @Test
    @DisplayName("Je teste la recherche d'adresse")
    void searchAddressTest() {

        String q = "4 rue saint thomas";

        webTestClient = WebTestClient.bindToServer()
                .baseUrl("https://api-adresse.data.gouv.fr")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .build();

        webTestClient.get().uri("/search/?q=" + q + "&limit=5&autocomplete=1")
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBodyList(SearchAdresse.class).hasSize(1);
    }
}
