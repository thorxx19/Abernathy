package com.abernathy.gateway.controller;


import com.abernathy.gateway.configuration.JwtTokenProvider;
import com.abernathy.gateway.controller.impl.AuthController;
import com.abernathy.gateway.model.AuthResponse;
import com.abernathy.gateway.model.RoleDto;
import com.abernathy.gateway.model.UserDto;
import com.abernathy.gateway.model.UserRequest;
import com.abernathy.gateway.service.impl.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author o.froidefond
 */

@Slf4j
@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = AuthController.class)
@Import(AuthService.class)
@ActiveProfiles("test")
public class AuthServiceTest {

    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private AuthService authService;
    @MockBean
    private RouteLocator routeLocator;
    @MockBean
    private IMicroserviceClient gatewayController;
    @MockBean
    private JwtTokenProvider jwtTokenProvider;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ReactiveAuthenticationManager authenticationManager;


    @Test
    @DisplayName("Je test le login echec")
    void loginTest() {

        UserRequest userRequest = new UserRequest();
        userRequest.setMail("lange@test.fr");
        userRequest.setPassword("Eagusto80608060#");

        UserDto userDto = new UserDto();
        RoleDto role = new RoleDto();
        List<RoleDto> roles = new ArrayList<>();

        role.setName("ADMIN");
        role.setId(UUID.randomUUID());
        roles.add(role);

        userDto.setId(UUID.randomUUID());
        userDto.setMail("lange@test.fr");
        userDto.setPassword("$2a$10$v6eg99D7N3VUNFFKpZTcqe7eLDLevOYT.E1NiMdc/");
        userDto.setName("Olivier");
        userDto.setLastName("FROIDEFOND");
        userDto.setRoles(roles);

        Mockito.when(gatewayController.findByMail(userRequest.getMail())).thenReturn(userDto);

        ResponseEntity<AuthResponse> authResponseResponseEntity = authService.login(userRequest);
        assertEquals("Mdp ou adresse mail non valide pour cette utilisateur", Objects.requireNonNull(authResponseResponseEntity.getBody()).getMessage());
    }

    @Test
    @DisplayName("Je test le login réussie")
    void loginTest1() {

        UserRequest userRequest = new UserRequest();
        userRequest.setMail("lange@test.fr");
        userRequest.setPassword("Eagusto80608060#");

        UserDto userDto = new UserDto();
        RoleDto role = new RoleDto();
        List<RoleDto> roles = new ArrayList<>();

        role.setName("ADMIN");
        role.setId(UUID.randomUUID());
        roles.add(role);

        userDto.setId(UUID.randomUUID());
        userDto.setMail("lange@test.fr");
        userDto.setPassword("$2a$10$v6eg99D7N3VUNFFKpZTcqe7eLDLevOYT.E1NiMdc/Jt3ZLm74Ci6e");
        userDto.setName("Olivier");
        userDto.setLastName("FROIDEFOND");
        userDto.setRoles(roles);

        Mockito.when(gatewayController.findByMail(userRequest.getMail())).thenReturn(userDto);

        ResponseEntity<AuthResponse> authResponseResponseEntity = authService.login(userRequest);
        assertEquals("Votre token", Objects.requireNonNull(authResponseResponseEntity.getBody()).getMessage());

    }

    @Test
    @DisplayName("Je test le register réussie")
    void registerTest() {

        UserDto userDto = new UserDto();

        userDto.setMail("lange@test.fr");
        userDto.setPassword("test@test.fr");
        userDto.setName("Olivier");
        userDto.setLastName("FROIDEFOND");


        Mockito.when(gatewayController.addProfil(userDto)).thenReturn(true);

        boolean response = authService.register(userDto);
        assertTrue(response);
    }

    @Test
    @DisplayName("Je test le register echec")
    void registerTest1() {

        UserDto userDto = new UserDto();

        userDto.setMail("lange@test.fr");
        userDto.setPassword("test@test.fr");
        userDto.setName("Olivier");
        userDto.setLastName("FROIDEFOND");


        Mockito.when(gatewayController.addProfil(userDto)).thenReturn(false);
        boolean response = authService.register(userDto);
        assertFalse(response);

    }

}
