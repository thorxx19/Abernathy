<img align="left" width="250" src="https://user.oc-static.com/upload/2023/08/09/16915753889047_fr_DA-JAVA_P7-P8-P9_P9-02%20%281%29%20Large.jpeg">

# MédiLabo

MédiLabo est un projet 'Maven' destiné au personnel soignant pour la gestion des risques au diabète.

## Composants du projet

### 1. Config Server
- Description : Service de configuration centralisé pour tous les microservices.

### 2. Eureka Server
- Description : Serveur de registre pour la découverte et l'enregistrement des microservices.

### 3. Microservice Patient
- Description : Gestion des informations et des données des patients.

### 4. Microservice Notes
- Description : Service de gestion des notes associées aux patients.

### 5. Gateway
- Description : Point d'entrée pour l'accès aux différents microservices.


## GitLab Back SpingBoot
Link [MédiLabo](https://gitlab.com/thorxx19/Abernathy.git)

## Installation MédiLabo

```bash
docker-compose up
```
# Enjeux du Green Code

## Pourquoi le Green Code est important

Le Green Code fait référence à l'approche de développement logiciel qui vise à réduire l'empreinte environnementale des applications et des systèmes informatiques. Cette pratique devient de plus en plus cruciale dans un contexte où la technologie joue un rôle central dans notre quotidien. Voici quelques enjeux importants :

### 1. Consommation énergétique
Les logiciels et les infrastructures informatiques peuvent avoir une empreinte énergétique considérable. Optimiser le code pour réduire la consommation d'énergie est essentiel pour contribuer à la lutte contre le changement climatique.

### 2. Utilisation efficace des ressources
Des algorithmes plus efficaces, des processus de développement plus durables et une utilisation judicieuse des ressources matérielles contribuent à minimiser les déchets et à préserver les ressources naturelles.

### 3. Impact sur le cycle de vie
La durabilité ne se limite pas seulement à la phase de développement. La maintenance, les mises à jour et la gestion du cycle de vie du logiciel doivent également être pensées de manière durable pour minimiser l'impact sur l'environnement.

## Recommandations pour un projet Green

Pour rendre ce projet plus respectueux de l'environnement, voici quelques recommandations à considérer :

### 1. Optimisation du code
Revoir le code existant pour identifier les opportunités d'optimisation, telles que la réduction des requêtes réseau, l'optimisation des algorithmes, et la minimisation de l'utilisation de la mémoire.

### 2. Utilisation de ressources écoénergétiques
Privilégier l'utilisation de serveurs ou de services hébergés alimentés par des sources d'énergie renouvelable pour réduire l'empreinte carbone de l'infrastructure.

### 3. Documentation des bonnes pratiques
Inclure dans ce fichier README.md des directives et des bonnes pratiques pour le développement durable, encourageant ainsi une culture de développement respectueuse de l'environnement.

### 4. Mesure de la performance énergétique
Intégrer des outils ou des métriques permettant de mesurer la consommation énergétique du logiciel afin de mieux comprendre et d'optimiser son impact.

### 5. Sensibilisation de l'équipe
Organiser des sessions de sensibilisation et de formation pour l'équipe de développement afin de promouvoir une prise de conscience collective sur l'importance du Green Code.

---
