import React from "react";
import { Route, Routes } from "react-router-dom";
import ListClient from "../page/ListClient";
import FicheClient from "../page/FicheClient";
import NoteClient from "../page/NoteClient";
import Page404 from "../page/Page404";
import Page503 from "../page/Page503";
import Shutdown from "../page/Shutdown";
import Admin from "../page/Admin";
import FicheUser from "../page/FicheUser";


const PageRouter = () => {
    return(
        <Routes>
            <Route index element={<ListClient/>} />
            <Route path="home" element={<ListClient/>} />
            <Route path="fiche" element={<FicheClient/>} />
            <Route path="fiche-user" element={<FicheUser/>}/>
            <Route path="note" element={<NoteClient/>} />
            <Route path="Page503" element={<Page503/>} />
            <Route path="Shutdown" element={<Shutdown/>} />
            <Route path="admin" element={<Admin/>} />
            <Route path="/*" element={<Page404/>}/>
        </Routes>
    );
};
export default PageRouter;