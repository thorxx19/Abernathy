import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import PageRouter from './router/PageRouter';
import LoginRouter from './router/LoginRouter';
import AuthGuard from './router/AuthGuard';

const App = () => {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/auth/*' element={<LoginRouter/>}/>
      <Route path='/*' element={
      <AuthGuard>
        <PageRouter/>
      </AuthGuard>
      }/>
    </Routes>
  </BrowserRouter>
  );
}

export default App;
