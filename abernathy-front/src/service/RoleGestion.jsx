import { jwtDecode } from "jwt-decode";


let getRoles = () => {
    let token = localStorage.getItem('token');
    if (!! token) {
        let decode = jwtDecode(token);
    return decode.roles[0]
    }
    return null
}
let getName = () => {
    let token = localStorage.getItem('token');
    if (!! token) {
        let decode = jwtDecode(token);
    return decode.sub
    }
    return null
}


export const rolesGestion = {
    getRoles, getName
}