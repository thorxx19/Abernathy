import Axios from "./caller";


// connection microservice patient
let deleteClient = (id) => {
    return Axios.delete('/client/' + id)
}
let getClient = (id) => {
    return Axios.get('/client/' + id)
}
let updateClient = (client) => {
    return Axios.put('/client', client)
}
let createClient = (client) => {
    return Axios.post('/client', client)
}
let getAllPagination = (page, size, filtre) => {
    return Axios.get('/client/pagination/all', {
        params:{
            page,
            size,
            filtre
        }
    })
}
// connection au microservice user
let getAllPaginationUser = (page, size, filtre) => {
    return Axios.get('/user/pagination/all', {
        params:{
            page,
            size,
            filtre
        }
    })
}

let getUserId = (id) => {
    return Axios.get('/user/' + id)
}
let updateUser = (user) => {
    return Axios.put('/user', user)
}
let deleteUser = (id) => {
    return Axios.delete('/user/' + id)
}
// connection au microservice roles

let getAllRoles = () => {
    return Axios.get("/roles/all")
}

// connection au microservice notes

let getAllPostPagination = (page, size, id) => {
    return Axios.get('/posts/pagination/all/' + id, {
        params:{
            page,
            size
        }
    })
}
let createNote = (note) => {
    return Axios.post("/posts", note)
}
let deleteNote = (id) => {
    return Axios.delete('/posts/' + id)
}
let getRisque = (genre, date, id) => {
    return Axios.get('/posts/risque/' + id, {
        params:{
            genre,
            date
        }
    })
}

// connection microservice gateway
let searchAdresse = (q) => {
    return Axios.get('/gateway/search-address', {
        params:{
            q
        }
    })
}

// connection Auth service

let login = (credentials) => {
    return Axios.post('/auth/login', credentials)
}
let register = (credentials) => {
    return Axios.post('/auth/register', credentials)
}


export const connectService = {
    deleteClient, getClient, updateClient, createClient, getAllPagination,
    getAllPostPagination, createNote, deleteNote, searchAdresse,
    login, register, getRisque, getAllPaginationUser, getUserId, updateUser,
    getAllRoles, deleteUser
}