import axios from "axios";
import { accountService } from "./ConnectService";


const Axios = axios.create({

    baseURL : "http://localhost:9004/api"
})
Axios.interceptors.request.use(request => {

    if (accountService.isLogged()){
        request.headers.Authorization = accountService.getToken()
    }

    return request;
})

Axios.interceptors.response.use(response => {
    return response;
})

export default Axios;