import React from "react";
import { NavLink } from "react-router-dom";
import { accountService } from "../service/ConnectService";
import { useEffect, useState } from "react";
import { rolesGestion } from "../service/RoleGestion";


const ButtonListAdmin = () => {

    const [role, setRole] = useState('');

    useEffect(() => {
        setRole(rolesGestion.getRoles())
    }, [])


    const logout = () => {
        accountService.logout()
    }

    return(
        <div className="row justify-content-between font my-2">
            <div className="col-4">
            </div>
            <div className="col-auto"> 
                {role !== 'ROLE_ADMIN' ? null : <NavLink to="/home" type="button" className="btn btn-outline-success my-2 mx-3">Home</NavLink>}
                <NavLink to="/auth/login" type="button" onClick={logout} className="btn btn-outline-danger my-2">Log Out</NavLink>
            </div>
        </div>
    )
};
export default ButtonListAdmin;