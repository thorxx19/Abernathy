import { React } from "react";
import { Form, Col } from 'react-bootstrap';



const AffichagePatientNote = (props) => {


    return(
        <>
            <Col>
                <Form.Label >First name</Form.Label>
                <Form.Control required type="text" placeholder="First name" defaultValue={props.firstName} disabled />
            </Col>
            <Col>
                <Form.Label >Last name</Form.Label>
                <Form.Control requiredtype="text" placeholder="Last name" defaultValue={props.name} disabled/>
            </Col>
            <Col>
                <Form.Label >Birth Date</Form.Label>
                <Form.Control type="date" placeholder="Last name" defaultValue={props.date} disabled/>
            </Col>
        </>
    )
};
export default AffichagePatientNote;