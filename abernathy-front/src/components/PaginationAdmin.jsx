import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";
import "../style/PaginationClient.css"

import Button from 'react-bootstrap/Button';
import { Paginator } from 'primereact/paginator';
import { useEffect, useState } from "react";
import { Col, Form, Modal} from "react-bootstrap";
import { connectService } from "../service/connection"
import { Link, useNavigate } from "react-router-dom";
import Toaster from "./Toaster";
import { rolesGestion } from "../service/RoleGestion";


const DataTablePaginationAdmin = () => {

    const navigation = useNavigate();
    const [datas, setDatas] = useState([])
    const [datasDelete, setDatasDelete] = useState([])
    const [showDelete, setShowDelete] = useState(false);
    const [color, setColor] = useState("danger")
    const [page, setPage] = useState(0);
    const [rows, setRows] = useState(5);
    const [pageCount, setPageCount] = useState(0)
    const [filtre, setFiltre] = useState('')
    const [show, setShow] = useState(false);
    const [idDelete, setIdDelete] = useState(0)
    const [role, setRole] = useState('');


    const handleClose = () => setShow(false);
    const handleShow = (event) => {
        setIdDelete(event.target.value)
        setShow(true)
    };

    const onPageChange = (event) => {
        setPage(event.page);
        setRows(event.rows);
        connectService.getAllPaginationUser(event.page, event.rows, filtre).then(data => {
            setDatas(data.data.content)
            setPageCount(data.data.totalElements)
        })
    };
    const onSearch = (event) => {
        setFiltre(event.target.value)
        connectService.getAllPaginationUser(page, rows, event.target.value).then(data => {
            setDatas(data.data.content)
            setPageCount(data.data.totalElements)
        })
    }

    useEffect(() => {

        setRole(rolesGestion.getRoles())

        connectService.getAllPaginationUser(page, rows, filtre).then(data => {
            if (data.status === 200) {
                setDatas(data.data.content)
                setPageCount(data.data.totalElements)
            }
        }).catch(error => {
            if (error.response != null) {
                if (error.response.status === 503) {
                    navigation('/Page503')
                }
                if (error.response.status === 401) {
                    navigation('auth/login')
                }
            }
        })
        
    }, [filtre, navigation, page, rows])

    const toaster = () => {
        setTimeout(() => {
            setShowDelete(true);
        }, 500);
        setTimeout(() => {
            setShowDelete(false)
        }, 3000)
    }
    const deleteUser = () => {
        
        if (idDelete !== 0) {
            setShow(false)
            connectService.deleteUser(idDelete).then(data => {
                if (data.request.status === 200) {
                    setPage(0)
                    toaster()
                    setDatasDelete(data.data.detail)
                    setColor("success")
                    connectService.getAllPaginationUser(page, rows, filtre).then(data => {
                        if (data.request.status === 200) {
                            setDatas(data.data.content)
                            setPageCount(data.data.totalElements)
                        }
                    })
                } 
           }).catch(error => {
                if (error.response.status === 404) {
                    toaster()
                    setDatasDelete(error.response.data.detail)
                    setColor("danger")
                }
           })
        }
    }
    
   
    return (
        <div>
            <Col md={4}>
                    <Form.Group>
                        <Form.Control aria-label="Small" type="text" defaultValue={filtre} placeholder="Recherche" name="name" onChangeCapture={onSearch}/>
                    </Form.Group>
            </Col>
          <div className="card my-5">
            <table className="table">
                <thead className="pagi">
                    <tr>
                    <th scope="col">Last Name</th>
                    <th scope="col">Name</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Role</th>
                    {role === 'ROLE_USER' ? null : <th></th>}
                    {role === 'ROLE_USER' ? null : <th></th>}
                    </tr>
                </thead>
                <tbody>
                    {datas.map((data, index) => (
                    <tr key={index}>
                        <td className="pagi15" style={{ width: "15%"}}>{data.lastName}</td>
                        <td className="pagi15" style={{ width: "15%" }}>{data.name}</td>
                        <td className="pagi15" style={{ width: "15%" }}>{data.mail}</td>
                        <td className="pagi25" style={{ width: "25%" }}>{data.roles[0].name.split('_')[1].toLowerCase()}</td>
                        {role === 'ROLE_USER' ? null : <td style={{ width: "5%" }}><Link to="/fiche-user" type="button" state={{ data: data.id }} className="btn btn-outline-success">Update</Link></td>}
                        {role === 'ROLE_USER' ? null : <td style={{ width: "5%" }}><Button variant="outline-danger" onClick={handleShow} value={data.id}>Delete</Button></td>} 
                    </tr>
                    ))}
                </tbody>
            </table>
            <Paginator first={page} rows={rows} pageLinkSize={pageCount} totalRecords={pageCount} rowsPerPageOptions={[5, 10, 15]} onPageChange={onPageChange} />
          </div>
          
          {/* TOASTER */}
            <Toaster bool={showDelete} message={datasDelete} color={color} />
          {/* TOASTER */}
          {/* MODAL */}
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
        <Modal.Title>Suppression</Modal.Title>
            </Modal.Header>
            <Modal.Body>Confirmation de suppression</Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={handleClose}>
                    Annuler
                </Button>
                <Button variant="outline-success" onClick={deleteUser}>
                    Confirmer
                </Button>
            </Modal.Footer>
        </Modal>
          {/* MODAL */}
      </div>
      );
};
export default DataTablePaginationAdmin;