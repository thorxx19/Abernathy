import React from "react";
import { NavLink } from "react-router-dom";
import { accountService } from "../service/ConnectService";
import { useEffect, useState } from "react";
import { rolesGestion } from "../service/RoleGestion";
import Badge from 'react-bootstrap/Badge';


const ButtonAddClient = () => {

    const [role, setRole] = useState('');
    const [name, setName] = useState('');
    const [roleModif, setRoleModif] = useState("");

    useEffect(() => {
        const roleString = rolesGestion.getRoles();
        const roleSplit = roleString.split('_');
        setRoleModif(roleSplit[1]);
        setRole(rolesGestion.getRoles())
        setName(rolesGestion.getName())
    }, [])


    const logout = () => {
        accountService.logout()
    }

    return(
        <div className="row justify-content-between font my-2">
            <div className="col-5">
            {role !== 'ROLE_ADMIN' && role !== 'ROLE_DOCTOR' ? null : <NavLink to="/fiche" type="button" className="btn btn-outline-success my-2 mx-2">Add</NavLink>}
            <Badge className="mx-1" bg="warning" text="dark">{name}</Badge>
            <Badge pill bg="dark">{roleModif}</Badge>
            </div>
            <div className="col-auto"> 
                {role !== 'ROLE_ADMIN' ? null : <NavLink to="/admin" type="button" className="btn btn-outline-info my-2 mx-3">Admin</NavLink>}
                <NavLink to="/auth/login" type="button" onClick={logout} className="btn btn-outline-danger my-2">Log Out</NavLink>
            </div>
        </div>
    )
};
export default ButtonAddClient;