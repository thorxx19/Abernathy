import { React, useEffect, useState } from "react";
import { Col, Container, FloatingLabel, Form, FormControl, Button, Row } from "react-bootstrap";
import { useLocation, useNavigate, Link } from "react-router-dom";
import { connectService } from "../service/connection"
import Toaster from "../components/Toaster";
import { AutoComplete } from "primereact/autocomplete";
import { rolesGestion } from "../service/RoleGestion";
import "../style/NoteClient.css"



const FicheClient = () => {

    const location = useLocation();
    const navigation = useNavigate();
    const dataIdClient = location.state?.data;
    const [datasAdd, setDatasAdd] = useState("")
    const [showAdd, setShowAdd] = useState(false);
    const [color, setColor] = useState("danger")
    const [errorForm, setErrorForm] = useState([]);
    const [items, setItems] = useState([]);
    

    const [credentials, setCredentials] = useState({
        id: "",
        name: "",
        firstName: "",
        address: "",
        numberPhone: "",
        birthDate:"",
        dateBirthDateFormat:"",
        genre:""
    });

    const toasterValid = () => {
        setTimeout(() => {
            setShowAdd(true);
        }, 500);
        setTimeout(() => {
            setShowAdd(false)
            navigation('/')
        }, 3000)
    }

    const toasterError = () => {
        setTimeout(() => {
            setShowAdd(true);
        }, 500);
        setTimeout(() => {
            setShowAdd(false)
        }, 3000)
    }

    const search = (e) => {
        connectService.searchAdresse(e.query).then(data => {
            setItems(data.data.map(items => items.label))
            data.data.map(items => items.label)
        })       
    }


    const onChange = (e) => {
        setCredentials({
            ...credentials,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = () => {
        if (credentials.id !== "") {
            connectService.updateClient(credentials).then(data => {
                if (data.request.status === 200) {
                    toasterValid()
                    setColor("success")
                    setDatasAdd(data.data.detail)
                }
            }).catch(error => {
                if (error.response.status === 404) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data.detail)
                }
                if (error.response.status === 400) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data.message)
                    setErrorForm(error.response.data.errors)
                }
            })
        } else {
            connectService.createClient(credentials).then(data => {
                if (data.request.status === 201) {
                    toasterValid()
                    setColor("success")
                    setDatasAdd(data.data.detail)
                }
            }).catch(error => {
                if (error.response.status === 404) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data.detail)
                }
                if (error.response.status === 400) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data.message)
                    setErrorForm(error.response.data.errors)
                }
            })
        }
    }
    
    useEffect(() => {

        let role = rolesGestion.getRoles()

        if (role === 'ROLE_ADMIN' || role === 'ROLE_DOCTOR') {
            if (location.state !== null) {
                connectService.getClient(dataIdClient).then(data => {
                    setCredentials(data.data)
                })
            }
        } else {
            navigation('/home')
        }
        
    }, [dataIdClient, location.state, navigation])

    return (
        <Container fluid="md" className="my-5">
            
            <Form>
            <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingName" label="Name" className="mb-3">
                            <FormControl type="text" className={errorForm.map((error) => (
                                error.field === 'name' ? "is-invalid" : "is-valid"))} placeholder="Name" name="name" defaultValue={credentials.name} onInput={onChange}></FormControl>
                            {errorForm.map((error, index) => (
                                error.field === 'name' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingFirstName" label="FirstName" className="mb-3">
                            <FormControl className={errorForm.map((error) => (
                                error.field === 'firstName' ? "is-invalid" : "is-valid"))} type="text" placeholder="FirstName" name="firstName" defaultValue={credentials.firstName} onInput={onChange}></FormControl>
                                {errorForm.map((error, index) => (
                                error.field === 'firstName' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5} className="mb-4">
                    <Form.Select className={errorForm.map((error) => (
                                error.field === 'genre' ? "is-invalid" : "is-valid"))} value={credentials.genre} name="genre" onChange={onChange} aria-label="Select Genre">
                        <option>Select Genre</option>
                        <option value="M">Homme</option>
                        <option value="F">Femme</option>
                    </Form.Select>
                    {errorForm.map((error, index) => (
                                error.field === 'genre' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <span className="p-float-label label">
                    <AutoComplete className="client" value={credentials.address} suggestions={items} name="address" completeMethod={search} onChange={onChange} />
                    <label htmlFor="ac" >Address</label>
                    </span>
                    {errorForm.map((error, index) => (
                                error.field === 'address' && <div className="error" key={index} >{error.message}</div>
                            ))}
                    
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5} className="mt-3">
                    <Form.Group>
                        <FloatingLabel controlId="floatingBirthDate" label="Birth Date" className="mb-3">
                            <FormControl type="date" className={errorForm.map((error) => (
                                error.field === 'birthDate' ? "is-invalid" : "is-valid"))} placeholder="Birth Date" name="birthDate" defaultValue={credentials.dateBirthDateFormat} onInput={onChange}></FormControl>
                                {errorForm.map((error, index) => (
                                error.field === 'birthDate' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingNumberPhone" label="Number Phone" className="mb-3">
                            <FormControl className={errorForm.map((error) => (
                                error.field === 'numberPhone' ? "is-invalid" : "is-valid"))} type="text" placeholder="Number Phone" name="numberPhone" defaultValue={credentials.numberPhone} onInput={onChange}></FormControl>
                                {errorForm.map((error, index) => (
                                error.field === 'numberPhone' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Button variant="success" className="mx-2" onClick={onSubmit}>Validation</Button>
                    <Link to="/" type="button" className="btn btn-danger mx-2">Annulation</Link>
                </Col>
                </Row>
            </Form>
            
            <Toaster bool={showAdd} color={color} message={datasAdd}/>
        </Container>
    );

}
export default FicheClient;