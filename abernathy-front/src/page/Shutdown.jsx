import Image from 'react-bootstrap/Image';
import shutdown from "../assets/shutdown.jpg"
import {Container, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";


const Shutdown = () => {

    return (
        <>
        <Container>
            <Row className="justify-content-md-center">
                <Col md="auto" className='my-5'><Link to="/" type="button" className="btn btn-outline-success">Retour</Link></Col>
            </Row>
            <Image src={shutdown} fluid />
        </Container>
        </>
    )
}
export default Shutdown;