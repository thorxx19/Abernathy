import Image from 'react-bootstrap/Image';
import image503 from "../assets/status_503-t.jpg"
import {Container, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";


const Page503 = () => {

    return (
        <>
        <Container>
            <Row className="justify-content-md-center">
                <Col md="auto" className='my-5'><Link to="/" type="button" className="btn btn-outline-success">Retour</Link></Col>
            </Row>
            <Image src={image503} fluid />
        </Container>
        </>
    )
}
export default Page503;