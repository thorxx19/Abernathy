import { React, useEffect, useState } from "react";
import { Col, Container, FloatingLabel, Form, FormControl, Button, Row } from "react-bootstrap";
import { useLocation, useNavigate, Link } from "react-router-dom";
import { connectService } from "../service/connection"
import Toaster from "../components/Toaster";
import { rolesGestion } from "../service/RoleGestion";
import "../style/NoteClient.css"



const FicheUser = () => {

    const [credentials, setCredentials] = useState({
        id: "",
        name: "",
        lastName: "",
        mail: "",
        roles:[{
            id:"",
            name:""
        }]
    });

    const location = useLocation();
    const navigation = useNavigate();
    const dataIdUser = location.state?.data;
    const [datasAdd, setDatasAdd] = useState([])
    const [showAdd, setShowAdd] = useState(false);
    const [color, setColor] = useState("danger")
    const [errorForm, setErrorForm] = useState([]);
    const [roles, setRoles] = useState([])

    const toasterValid = () => {
        setTimeout(() => {
            setShowAdd(true);
        }, 500);
        setTimeout(() => {
            setShowAdd(false)
            navigation('/admin')
        }, 3000)
    }

    const toasterError = () => {
        setTimeout(() => {
            setShowAdd(true);
        }, 500);
        setTimeout(() => {
            setShowAdd(false)
        }, 3000)
    }

    
    const onChange = (e) => {
        const {name, value} = e.target;
        if (name === "roles") {
            if (value !== "") {
            const updatedRoles = [...credentials.roles];
            updatedRoles[0] = {...updatedRoles[0],id: value,name:""};
            setCredentials({...credentials,roles: updatedRoles,});
            } else {
            setCredentials({...credentials,roles: [],});
            }
            
        } else {
            setCredentials({
                ...credentials,
                [e.target.name]: value,
            });
        }
    };

    const onSubmit = () => {
        if (credentials.id !== "") {
            connectService.updateUser(credentials).then(data => {
                if (data.request.status === 200) {
                    toasterValid()
                    setColor("success")
                    setDatasAdd(data.data)
                }
            }).catch(error => {
                if (error.response.status === 404) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data)
                }
                if (error.response.status === 400) {
                    toasterError()
                    setColor("danger")
                    setDatasAdd(error.response.data)
                    setErrorForm(error.response.data.errors)
                }
            })
        }
    }
    
    useEffect(() => {

        let role = rolesGestion.getRoles()

        if (role === 'ROLE_ADMIN' || role === 'ROLE_DOCTOR') {
            if (location.state !== null) {
                connectService.getAllRoles().then(data => {
                    setRoles(data.data)
                })
                connectService.getUserId(dataIdUser).then(data => {
                    setCredentials(data.data)
                })
            }
        } else {
            navigation('/home')
        }
        
    }, [dataIdUser, location.state, navigation])

    return (
        <Container fluid="md" className="my-5">
            
            <Form>
            <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingName" label="Name" className="mb-3">
                            <FormControl type="text" className={errorForm.map((error) => (
                                error.field === 'name' ? "is-invalid" : "is-valid"))} placeholder="Name" name="name" defaultValue={credentials.name} onInput={onChange}></FormControl>
                            {errorForm.map((error, index) => (
                                error.field === 'name' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingLastName" label="LastName" className="mb-3">
                            <FormControl className={errorForm.map((error) => (
                                error.field === 'lastName' ? "is-invalid" : "is-valid"))} type="text" placeholder="LastName" name="lastName" defaultValue={credentials.lastName} onInput={onChange}></FormControl>
                                {errorForm.map((error, index) => (
                                error.field === 'lastName' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5} className="mb-4">
                    <Form.Select className={errorForm.map((error) => (
                                error.field === 'roles' ? "is-invalid" : "is-valid"))} value={credentials.roles.length > 0 ? credentials.roles[0].id : ''} name="roles" onChange={onChange} aria-label="Select Role">
                        <option value="">Select Role</option>
                        {roles.map(role => (
                            <option key={role.id} value={role.id}>{role.name.split('_')[1].toLowerCase()}</option>
                        ))}  
                    </Form.Select>
                    {errorForm.map((error, index) => (
                                error.field === 'roles' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Form.Group>
                        <FloatingLabel controlId="floatingMail" label="Mail" className="mb-3">
                            <FormControl className={errorForm.map((error) => (
                                error.field === 'mail' ? "is-invalid" : "is-valid"))} type="text" placeholder="Mail" name="Mail" defaultValue={credentials.mail} onInput={onChange}></FormControl>
                                {errorForm.map((error, index) => (
                                error.field === 'mail' && <div key={index} className="invalid-feedback">{error.message}</div>
                            ))}
                        </FloatingLabel>
                    </Form.Group>
                </Col>
                </Row>
                <Row className="justify-content-md-center">
                <Col md={5}>
                    <Button variant="success" className="mx-2" onClick={onSubmit}>Validation</Button>
                    <Link to="/admin" type="button" className="btn btn-danger mx-2">Annulation</Link>
                </Col>
                </Row>
            </Form>
            
            <Toaster bool={showAdd} color={color} message={datasAdd.message}/>
        </Container>
    );

}
export default FicheUser;