import Image from 'react-bootstrap/Image';
import image404 from "../assets/romson-preechawit-Vy2cHqm0mCs-unsplash.jpg"
import {Container, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";


const Page404 = () => {

    return (
        <>
        <Container>
            <Row className="justify-content-md-center">
                <Col md="auto" className='my-5'><Link to="/" type="button" className="btn btn-outline-success">Retour</Link></Col>
            </Row>
            <Image src={image404} fluid />
        </Container>
        </>
    )
}
export default Page404;