import React from "react";
import { Container } from "react-bootstrap";
import ButtonAddClient from "../components/ButtonAddClient";
import DataTablePagination from "../components/Pagination";

function ListClient() {

    return(
        <Container fluid="md" className="justify-content-md-center">
            <ButtonAddClient />
            <DataTablePagination />
        </Container>
    );
}
export default ListClient;