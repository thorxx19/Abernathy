import { React, useEffect, useState } from "react";
import { Container, Form, InputGroup, Button, Card, Badge, ProgressBar } from 'react-bootstrap';
import Stack from 'react-bootstrap/Stack';
import { Link, useLocation, useNavigate } from "react-router-dom";
import { connectService } from "../service/connection"
import AffichagePatientNote from "../components/AffichagePatientNote";
import { useForm } from "react-hook-form";
import { Paginator } from 'primereact/paginator';
import "../style/NoteClient.css"
import { rolesGestion } from "../service/RoleGestion";
import moment from 'moment';



const NoteClient = () => {

    const navigation = useNavigate();
    const location = useLocation();
    const dataIdClient = location.state?.data;
    const [page, setPage] = useState(0);
    const [rows, setRows] = useState(2);
    const [datas, setDatas] = useState([])
    const [totalElement, setTotalElement] = useState(0)
    const [now, setNow] = useState(25);
    const [classN, setClassN] = useState("progress-bar-green")

    
    const [credentials, setCredentials] = useState({
        id: "",
        name: "",
        firstName: "",
        address: "",
        numberPhone: "",
        birthDate:"",
        dateBirthDateFormat:"",
        genre:""
    });

    const handleRiskData = (data, setNow, setClassN) => {
        if (data === "None") {
          setNow(25);
          setClassN("progress-bar-green");
        } else if (data === "Borderline") {
          setNow(50);
          setClassN("progress-bar-yellow");
        } else if (data === "In Danger") {
          setNow(75);
          setClassN("progress-bar-orange");
        } else if (data === "Early onset") {
          setNow(100);
          setClassN("progress-bar-red");
        }
      };

    const { register, handleSubmit, reset } = useForm({
        defaultValues: {
            idPatient: dataIdClient,
            name: '',
            content: ''
        }
    });

    const onSubmit = data => {
        if (data.name !== '' && data.content !== '') {
            connectService.createNote(data).then(data => {
                if (data.request.status === 200) {
                    connectService.getAllPostPagination(page, rows, dataIdClient).then(data => {
                        setDatas(data.data.content)
                        setTotalElement(data.data.totalElements)
                        reset({
                            idPatient: dataIdClient,
                            name: '',
                            content: ''
                        })
                    })
                    connectService.getRisque(credentials.genre, credentials.birthDate, credentials.id).then(data=> {
                        handleRiskData(data.data, setNow, setClassN)
                    })
                }
            })
        }
    };

    

    const onPageChange = (event) => {
        setPage(event.page);
        setRows(event.rows);
        connectService.getAllPostPagination(event.page, event.rows, dataIdClient).then(data => {
            setDatas(data.data.content)
            setTotalElement(data.data.totalElements)
        })
    };

    const onDelete = (event) => {
        connectService.deleteNote(event.target.value).then(data => {
            if (data.request.status === 200) {
                setPage(0)
                connectService.getAllPostPagination(page, rows, dataIdClient).then(data => {
                    setDatas(data.data.content)
                    setTotalElement(data.data.totalElements)
                })
                connectService.getRisque(credentials.genre, credentials.birthDate, credentials.id).then(data=> {
                    handleRiskData(data.data, setNow, setClassN)
                })
            }
        })
    };


    useEffect(() => {
        if (rolesGestion.getRoles() === "ROLE_USER") {
            navigation('/Page503')
        }
        if (location.state !== null) {
            
            connectService.getClient(dataIdClient).then(data => {

                connectService.getRisque(data.data.genre, data.data.birthDate, data.data.id).then(data=> {
                    handleRiskData(data.data, setNow, setClassN)
                })
                setCredentials(data.data)
            })
        }
        connectService.getAllPostPagination(page, rows, dataIdClient).then(data => {
            setDatas(data.data.content)
            setTotalElement(data.data.totalElements)
        }).catch(error => {
            console.error(error);
            if (error.response.status === 503) {
                navigation('/Page503')
            }
        })
    }, [dataIdClient, location.state, page, rows, navigation])

    
    return(
        <Container  fluid="md" className="my-5">
        <Stack direction="horizontal" gap={3} className="my-2 font1">
            <Link to="/" type="button" className="btn btn-outline-danger">Retour</Link>
            <div className="vr" />
            <AffichagePatientNote name={credentials.name} firstName={credentials.firstName} date={credentials.dateBirthDateFormat}/>
            <div className="vr" />
        </Stack>
        <ProgressBar className="my-2">
            <ProgressBar animated className={classN} now={now}/>
        </ProgressBar>
        
        {/* Formulaire dans note */}
        <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Select {...register("name")} aria-label="Default select example">
                <option value="">Open this select menu</option>
                <option value="RDV">Rdv cabinet</option>
                <option value="TELE">Téléconsultation</option>
                <option value="AUTRE">Autre</option>
            </Form.Select>
            <InputGroup>
                    <InputGroup.Text>Note Patient</InputGroup.Text>
                    <Form.Control as="textarea" aria-label="With textarea" {...register("content")}/>
                    <Button variant="success" type='submit'>Sauvegarder</Button>
            </InputGroup>
        </Form>
        {/* Pagination dans note */}

    
        {datas.map((data, index) => (
                <Card key={index} className="my-2">
                <Card.Header >
                <Stack direction="horizontal" gap={3}>
                        <div className="p-2">{moment(data.date).format('DD/MM/YYYY HH:mm')}</div>
                        <div className="vr" />
                        <div className="p-2 ms-auto"><Button variant="outline-danger" onClick={onDelete} value={data.id}>Delete</Button></div>
                </Stack>    
                </Card.Header>
                <Card.Body>
                    <blockquote className="blockquote mb-0">
                    <footer className="blockquote">
                        <h5><Badge bg="success">{data.name}</Badge></h5>
                    </footer>
                    <p>
                        {' '}
                        {data.content}
                        {' '}
                    </p>
                    </blockquote>
                </Card.Body>
                </Card>
            ))}
            <Paginator first={page} rows={rows} totalRecords={totalElement} rowsPerPageOptions={[2, 4, 6]} onPageChange={onPageChange} />
        </Container>
    )
}
export default NoteClient;




