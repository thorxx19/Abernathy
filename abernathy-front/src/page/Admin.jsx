import { Container } from 'react-bootstrap';
import DataTablePaginationAdmin from '../components/PaginationAdmin';
import ButtonListAdmin from '../components/ButtonListAdmin';
import { React, useEffect } from "react";
import { rolesGestion } from "../service/RoleGestion";
import { useNavigate } from "react-router-dom";


const Admin = () => {
    const navigation = useNavigate();

    useEffect(() => {

        let role = rolesGestion.getRoles()
    
        if (role !== 'ROLE_ADMIN') {
            navigation('/home')
        }
        
    }, [navigation])

    return(
        <>
        <Container fluid="md" className="justify-content-md-center">
            <ButtonListAdmin/>
            <DataTablePaginationAdmin/>
        </Container>
        </>
    )
}
export default Admin;
