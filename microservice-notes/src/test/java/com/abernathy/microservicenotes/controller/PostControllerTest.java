package com.abernathy.microservicenotes.controller;


import com.abernathy.microservicenotes.dao.IPostDao;
import com.abernathy.microservicenotes.model.Post;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author o.froidefond
 */
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
@ActiveProfiles("test")
public class PostControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private IPostDao postDao;

    private Post note;

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        Post note = new Post();
        note.setDate(new Date());
        note.setName("TEST");
        note.setContent("Test sauvegarde");
        note.setIdPatient(UUID.randomUUID());
        this.note = note;
    }

    @Test
    @DisplayName("Je test la sauvegarde d'une notes")
    void createNotesTest() {

        try {
            mockMvc.perform(post("/api/posts").contentType(MediaType.APPLICATION_JSON).content(asJsonString(note)))
                    .andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
            log.error("Error : ", e);
        }

    }

    @Test
    @DisplayName("Je test la suppression d'une note")
    void deleteNoteTest() {
        postDao.save(note);
        try {
            mockMvc.perform(delete("/api/posts/{id}", note.getId()).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test la récupération d'une liste de notes paginer en fonction d'un id client")
    void findAllPostTest() {
        postDao.save(note);
        int page = 0;
        int size = 5;

        try {
            mockMvc.perform(get("/api/posts/pagination/all/{id}", note.getIdPatient())
                            .param("page", String.valueOf(page))
                            .param("size", String.valueOf(size))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath("$.content[0].content").value("Test sauvegarde"))
                    .andExpect(jsonPath("$.content[0].name").value("TEST")).andReturn();
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test la supression de toutes les notes lier a un client")
    void deleteAllNoteByIdPatientTest() {
        postDao.save(note);
        try {
            mockMvc.perform(delete("/api/posts/all/{id}", note.getIdPatient())
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un homme de 1981")
    void infoRisqueTest1() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "M")
                            .param("date", "1981-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("Borderline"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'une femme de 1981")
    void infoRisqueTest2() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "F")
                            .param("date", "1981-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("Borderline"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'une femme de 1991")
    void infoRisqueTest3() {
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "F")
                            .param("date", "1991-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("None"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un homme de 1991")
    void infoRisqueTest4() {
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "M")
                            .param("date", "1991-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("None"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un homme de 2001")
    void infoRisqueTest5() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre a cause d'anticorp et de sont cholestérol");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "M")
                            .param("date", "2001-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("In Danger"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'une femme de 1991")
    void infoRisqueTest6() {
        note.setContent("Mon patient est fumeuse et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre du au anticorps, cholestérol et hémoglobine A1C");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "F")
                            .param("date", "1991-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("In Danger"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un homme de 2001")
    void infoRisqueTest7() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre a cause d'anticorp, cholestérol, Microalbumine, Hémoglobine A1C, sa taille et sont poids");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "M")
                            .param("date", "1991-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("Early onset"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un femme de 2001")
    void infoRisqueTest8() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre a cause d'anticorp, cholestérol, Microalbumine, Hémoglobine A1C, sa taille et sont poids");
        postDao.save(note);

        MvcResult mvcResult;
        try {

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "F")
                            .param("date", "1991-12-20T00:00:00.000+00:00")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("Early onset"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un femme de 1991")
    void infoRisqueTest9() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre a cause d'anticorp, cholestérol, Microalbumine, Hémoglobine A1C, sa taille et sont poids");
        postDao.save(note);

        MvcResult mvcResult;
        try {
            DateTime dateTime = new DateTime().plusDays(1).minusYears(32).toDateTimeISO();

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "F")
                            .param("date", String.valueOf(dateTime))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("Early onset"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @Test
    @DisplayName("Je test le calcule de risque d'un femme de 1991")
    void infoRisqueTest10() {
        note.setContent("Mon patient est fumeur et a eu des vertiges du a la réaction d'un médicament. Une rechute et a craindre a cause d'anticorp, cholestérol, Microalbumine, Hémoglobine A1C, sa taille et sont poids");
        postDao.save(note);

        MvcResult mvcResult;
        try {
            DateTime dateTime = new DateTime().plusDays(1).minusYears(32).toDateTimeISO();

            mvcResult = mockMvc.perform(get("/api/posts/risque/{id}", note.getIdPatient())
                            .param("genre", "N")
                            .param("date", String.valueOf(dateTime))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("None"));
        } catch (Exception e) {
            log.error("Error : ", e);
        }
    }

    @AfterEach
    void setupDelete() {
        postDao.deleteByIdPatient(note.getIdPatient());
    }

}
