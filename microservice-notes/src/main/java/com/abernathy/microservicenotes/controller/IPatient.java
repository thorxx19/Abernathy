package com.abernathy.microservicenotes.controller;

import com.abernathy.microservicenotes.model.Client;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@FeignClient(value = "gateway")
public interface IPatient {

    @GetMapping("microservice-patient/api/client/{id}")
    Optional<Client> findByIdClient(@PathVariable("id") UUID id);

}
