package com.abernathy.microservicenotes.controller;

import com.abernathy.microservicenotes.model.Post;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.UUID;


/**
 * @author o.froidefond
 */
public interface IPostController {

    void postNote(Post note);

    void deleteNote(String id);

    Page<Post> findAllPost(int page, int size, UUID id);

    boolean deleteAllNoteByIdPatient(UUID id);

    String infoRisque(String genre, Date date, UUID id);
}
