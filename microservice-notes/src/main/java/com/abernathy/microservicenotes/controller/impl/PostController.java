package com.abernathy.microservicenotes.controller.impl;

import com.abernathy.microservicenotes.controller.IPostController;
import com.abernathy.microservicenotes.model.Post;
import com.abernathy.microservicenotes.service.IPostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;


/**
 * @author o.froidefond
 */
@RestController
@Tag(name = "Posts", description = "Gestion des Posts")
@RequestMapping("/api/posts")
@Slf4j
public class PostController implements IPostController {

    @Autowired
    private IPostService postService;

    /**
     * méthode pour sauver une notes
     *
     * @param note une note a sauvegarder
     */
    @Operation(summary = "Création d'un post", description = "Création d'un post")
    @Override
    @PostMapping
    public void postNote(@RequestBody Post note) {
        postService.save(note);
    }

    /**
     * méthode pour supprimer une note
     *
     * @param id l'id d'une note
     */
    @Operation(summary = "Suppression d'une note", description = "Suppression d'une note")
    @Override
    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable String id) {
        postService.delete(id);
    }

    /**
     * méthode qui retourne une liste paginer de notes
     *
     * @param page la page
     * @param size la taille de la page
     * @param id   l'id du patient
     * @return une liste paginer
     */
    @Operation(summary = "Retourne une liste de post paginer", description = "Retourne une liste de post paginer")
    @Override
    @GetMapping("/pagination/all/{id}")
    public Page<Post> findAllPost(@RequestParam int page, int size, @PathVariable UUID id) {
        return postService.getPostPagination(id, page, size);
    }

    /**
     * méthode qui permet de delete toutes les notes associer a un patient
     *
     * @param id du patient
     * @return un boolean
     */
    @Operation(summary = "Permet de supprimer toute les notes suite d'un patient", description = "Permet de supprimer toute les notes suite d'un patient")
    @Override
    @DeleteMapping("/all/{id}")
    public boolean deleteAllNoteByIdPatient(@PathVariable UUID id) {
        return postService.deleteAllNoteByIdPatient(id);
    }

    /**
     * méthode qui permet de calculer les risque de diabéte pour un patient
     *
     * @param genre le sex du patient
     * @param date  la date de naissance du patient
     * @param id    du patient
     * @return un risque
     */
    @Operation(summary = "Permet de calculer le risque du patient", description = "Permet de calculer le risque de diabéte du patient")
    @GetMapping("risque/{id}")
    @Override
    public String infoRisque(@RequestParam String genre, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date date, @PathVariable UUID id) {
        return postService.algoCalculRisqueDiabete(genre, date, id);
    }

}
