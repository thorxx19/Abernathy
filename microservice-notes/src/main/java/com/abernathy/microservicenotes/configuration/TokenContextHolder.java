package com.abernathy.microservicenotes.configuration;

/**
 * @author o.froidefond
 */
public class TokenContextHolder {

    private static final ThreadLocal<String> TOKEN_HOLDER = new ThreadLocal<>();

    public static String getToken() {
        return TOKEN_HOLDER.get();
    }

    public static void setToken(String token) {
        TOKEN_HOLDER.set(token);
    }

    public static void clearToken() {
        TOKEN_HOLDER.remove();
    }

}
