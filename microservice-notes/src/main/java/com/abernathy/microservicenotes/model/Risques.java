package com.abernathy.microservicenotes.model;


/**
 * @author o.froidefond
 */
public enum Risques {

    NONE("None"),
    BODERLINE("Borderline"),
    IN_DANGER("In Danger"),
    EARLY_ONSET("Early onset");


    private final String valeur;

    Risques(String valeur) {
        this.valeur = valeur;
    }

    public String getValeur() {
        return valeur;
    }

}
