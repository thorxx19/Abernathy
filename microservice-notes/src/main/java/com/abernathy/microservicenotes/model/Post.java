package com.abernathy.microservicenotes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

/**
 * @author o.froidefond
 */
@Document(collection = "posts")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@CompoundIndex(name = "idPatient_idx", def = "{id : 1 , content : 1}")
public class Post {

    @Id
    private String id;
    private UUID idPatient;
    private String name;
    @TextIndexed
    private String content;
    private Date date;
    private Double score;

}
