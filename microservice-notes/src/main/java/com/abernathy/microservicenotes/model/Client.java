package com.abernathy.microservicenotes.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * @author o.froidefond
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Client {

    private UUID id;
    private String firstName;
    private String name;
    private Date birthDate;
    private String dateBirthDateFormat;
    private String genre;
    private String address;
    private String numberPhone;

}
