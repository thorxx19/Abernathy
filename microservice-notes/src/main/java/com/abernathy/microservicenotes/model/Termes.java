package com.abernathy.microservicenotes.model;

/**
 * @author o.froidefond
 */
public enum Termes {

    HEMOGLOBINE_A1C("Hémoglobine A1C"),
    MICROALBUMINE("Microalbumine"),
    TAILLE("Taille"),
    POIDS("Poids"),
    FUMEUR("Fumeur"),
    FUMEUSE("Fumeuse"),
    ANORMAL("Anormal"),
    CHOLESTEROL("Cholestérol"),
    VERTIGES("Vertiges"),
    RECHUTE("Rechute"),
    REACTION("Réaction"),
    ANTICORPS("Anticorps");


    private String valeur;

    Termes(String valeur) {
        this.valeur = valeur;
    }

    public String getValeur() {
        return valeur;
    }


}
