package com.abernathy.microservicenotes.service.impl;


import com.abernathy.microservicenotes.controller.IPatient;
import com.abernathy.microservicenotes.dao.IPostDao;
import com.abernathy.microservicenotes.model.Post;
import com.abernathy.microservicenotes.model.Risques;
import com.abernathy.microservicenotes.model.Termes;
import com.abernathy.microservicenotes.service.IPostService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * @author o.froidefond
 */
@Service
@Slf4j
public class PostService implements IPostService {


    @Autowired
    private IPostDao postDao;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private IPatient patient;

    @PostConstruct
    public void createTextIndex() {
        TextIndexDefinition textIndex = new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onField("content")
                .build();
        mongoTemplate.indexOps(Post.class).ensureIndex(textIndex);
    }

    /**
     * méthode pour sauver une notes
     *
     * @param note une note a sauvegarder
     */
    @Override
    public void save(Post note) {
        note.setDate(new Date());
        postDao.save(note);
    }

    /**
     * méthode pour supprimer une note
     *
     * @param id l'id d'une note
     */
    @Override
    public void delete(String id) {
        postDao.deleteById(id);
    }

    /**
     * méthode qui retourne une liste paginer de notes
     *
     * @param page la page
     * @param size la taille de la page
     * @param id   l'id du patient
     * @return une liste paginer
     */
    @Override
    public Page<Post> getPostPagination(UUID id, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("date").descending());
        return postDao.findByIdPatient(id, pageable);
    }

    /**
     * méthode qui permet de delete toutes les notes associer a un patient
     *
     * @param id du patient
     * @return un boolean
     */
    @Override
    public boolean deleteAllNoteByIdPatient(UUID id) {
        try {
            postDao.deleteByIdPatient(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    /**
     * méthode qui permet de calculer les risque de diabéte pour un patient
     *
     * @param genre le sex du patient
     * @param date  la date de naissance du patient
     * @param id    du patient
     * @return un risque
     */
    @Override
    public String algoCalculRisqueDiabete(String genre, Date date, UUID id) {

        List<String> allTermVariations = getAllVariations();

        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingAny(allTermVariations.toArray(new String[0])).caseSensitive(false).diacriticSensitive(false);

        TextQuery query = TextQuery.queryText(criteria).sortByScore().includeScore();
        query.addCriteria(Criteria.where("idPatient").is(id));
        List<Post> postRecup = mongoTemplate.find(query, Post.class);


        return calculRisque(postRecup, genre, date);
    }


    //**************************************
    // Private method
    //**************************************

    /**
     * méthode privée pour calculer les risque
     *
     * @param posts une liste de poste qui contienne un certain type de mot clé
     * @param genre le sex du patient
     * @param date  la date de naissance du patient
     * @return un string
     */
    private String calculRisque(List<Post> posts, String genre, Date date) {

        int agePatient = convertDate(date);

        int matchingWordCount = getMatchingWordCount(posts);

        switch (genre) {
            case "M": {
                if (posts.isEmpty() || matchingWordCount <= 2) {
                    return Risques.NONE.getValeur();
                } else if (matchingWordCount >= 3 && matchingWordCount < 5 && agePatient > 30) {
                    return Risques.BODERLINE.getValeur();
                } else if ((matchingWordCount >= 6 && matchingWordCount <= 7 && agePatient > 30) || (agePatient < 30 && matchingWordCount >= 3)) {
                    return Risques.IN_DANGER.getValeur();
                } else if ((matchingWordCount >= 8 && agePatient > 30) || (agePatient < 30 && matchingWordCount >= 5)) {
                    return Risques.EARLY_ONSET.getValeur();
                }
            }
            case "F": {
                if (posts.isEmpty() || matchingWordCount <= 2) {
                    return Risques.NONE.getValeur();
                } else if (matchingWordCount >= 3 && matchingWordCount <= 5 && agePatient > 30) {
                    return Risques.BODERLINE.getValeur();
                } else if ((matchingWordCount >= 6 && matchingWordCount <= 7 && agePatient > 30) || (agePatient < 30 && matchingWordCount == 4)) {
                    return Risques.IN_DANGER.getValeur();
                } else if ((matchingWordCount >= 8 && agePatient > 30) || (agePatient < 30 && matchingWordCount >= 7)) {
                    return Risques.EARLY_ONSET.getValeur();
                }
            }
            default: {
                return Risques.NONE.getValeur();
            }
        }
    }

    /**
     * méthode qui permet de matcher certain mot avec les posts
     *
     * @param posts une liste de posts
     * @return un entier
     */
    private int getMatchingWordCount(List<Post> posts) {
        List<String> searchWords = getAllVariations();
        int matchingWordCount = 0;
        List<String> definedWords = new ArrayList<>();
        for (Post post : posts) {
            String content = post.getContent();
            if (content.toLowerCase().contains(Termes.HEMOGLOBINE_A1C.getValeur().toLowerCase())) {
                matchingWordCount++;
            }

            String[] wordsInContent = content.split("\\s+|\\.|\\;|\'|,|( |,)");
            for (String wordSearch : searchWords) {
                for (String word : wordsInContent) {
                    if (word.toLowerCase().startsWith(wordSearch.toLowerCase())) {
                        matchingWordCount++;
                        definedWords.add(wordSearch);
                    }
                }
            }
        }
        return matchingWordCount;
    }

    /**
     * méthode pour convertir une date de naissance en age
     *
     * @param dateOfBirth un date de naissance
     * @return un entier
     */
    private int convertDate(Date dateOfBirth) {
        // Date actuelle
        Date currentDate = new Date();
        Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.setTime(dateOfBirth);
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(currentDate);

        int age = currentCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);

        // Vérifiez si le mois de naissance est après le mois actuel
        int moisNaissance = birthCalendar.get(Calendar.MONTH);
        int moisActuel = currentCalendar.get(Calendar.MONTH);

        if (moisActuel < moisNaissance ||
                (moisActuel == moisNaissance && currentCalendar.get(Calendar.DAY_OF_MONTH) < birthCalendar.get(Calendar.DAY_OF_MONTH))) {
            age--;
        }

        return age;
    }

    private List<String> getAllVariations() {
        List<String> variations = new ArrayList<>();
        for (Termes terme : Termes.values()) {
            String valeur = terme.getValeur();
            variations.add(valeur);

            // Vérification pour ajouter les suffixes alternatifs
            if (terme.equals(Termes.ANORMAL)) {
                variations.add(valeur + "e"); // ajout e
                variations.add(valeur + "es"); // ajout es
            }
        }
        return variations;
    }

}
