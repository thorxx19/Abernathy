package com.abernathy.microservicenotes.service;

import com.abernathy.microservicenotes.model.Post;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.UUID;


/**
 * @author o.froidefond
 */
public interface IPostService {

    void save(Post note);

    void delete(String id);

    Page<Post> getPostPagination(UUID id, int page, int size);

    boolean deleteAllNoteByIdPatient(UUID id);

    String algoCalculRisqueDiabete(String genre, Date date, UUID id);
}
