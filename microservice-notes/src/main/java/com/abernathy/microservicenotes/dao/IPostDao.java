package com.abernathy.microservicenotes.dao;

import com.abernathy.microservicenotes.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author o.froidefond
 */
@Repository
public interface IPostDao extends MongoRepository<Post, String> {
    /**
     * Méthode pour delete les notes en fonction de l'id d'un patient.
     *
     * @param idPatient id du patient.
     */
    void deleteByIdPatient(UUID idPatient);

    /**
     * Méthode pour trouver toutes les notes lier a un patient
     *
     * @param idPatient id du patient
     * @param pageable  pagination
     * @return une liste de notes paginer
     */
    Page<Post> findByIdPatient(UUID idPatient, Pageable pageable);
}
